Role Name
=========

Playbook for the SAP-Linux Installation on Rhel7 only the requirements!

#############################################################################################################
IMPORTANT: Bevor you run the playbook
-------------------------------------
 - add the host or the IP-Address in the nfs export file of the NetApp. 
 - add the Sap-repo to the host repolist (Repo Name: Red Hat Enterprise Linux for SAP (RHEL 7 Server) (RPMs))
#############################################################################################################

Requirements
------------

1.	Centralinstance:

-	Hostname „lx700YY“ 
-	Cname „uxsap<sid>z
-	User: <SID>adm, sapadm, smdadm, ysrvft1
-	Groups: sapsys, sapinst, ysrvft1
-	Disks: Swap-Disk (3*Memory), App-Disk (16G)
-	Filesystem: /usr/sap
-	Swap: lvswap
-	NFS Shares: /usr/sap/<SID> , /usr/sapbatch/<SID>, trans, shuttle, sapinstall
-	Firewall-Rules
-	SAP-Channel enable
-	System Software Requirement Install 
-	UC4-Agenten
-	Printer (Cups)
-	IPA-Anbindung ( Ansible?) - HBAC - Hostgroups
-	Config-Files (/etc/services, /etc/security/limits.d/sap.conf, /etc/sysctl.conf  …)
-	add the new service to /etc/services
-	Start und Stopscript
-	Add the automic-agents-playbooks

Dialoginstance removed. HeikoH 22.08.2018

Dependencies
------------

automic-agents-playbooks
