Usage of project roles:
-----------------------

install - installing a basic solr/jdk environment

sitecore - isntalling sitecore relevant solr cores

sitecore_replication - configuring replication of solr cores

update - updating solr and migrating existing cores

Initial sequence to install: install -> sitecore -> sitecore_replication -> update
---------------------------------------------------
