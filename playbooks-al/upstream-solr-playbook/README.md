Playbook to install Upstream Solr
---------------------------------

This Playbook is used to install the java based upstream apache project solr (a lucene based search engine with a lot more features)
as well as the needed Oracle JDK.

The Tower Survey asks for
- action to take
- the stage to deploy
- the Java heap size according to solr needs
- the name of the installation tarball
- if a master is to be installed (replication of search cores)
- the IP of the master
- the name of the JDK tarball
- the RHV SD to use for the application disk
- the size of the solr filesystem

A search core list is maintained in the pb itself in ./roles/APPLIKATION/vars/cores.yml!!

Please pay attention to additional README_roles.md in roles dir!!
