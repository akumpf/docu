#!/bin/bash

#twinter 2018-03-29

function prepare_html {

#print out the whole html block
cat <<EOF
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Linux Patchmanagement Uebersicht </title>

<style>

table {
        background-color: #f2f2f2;
        font-family: Arial, Verdana, sans-serif;
        border-collapse: collapse;
        width: 45%;
        font-size: 14px;
        color: #000000;
}

h1 { font-family: Arial, Verdana, sans-serif; font-size: 26px; color: #000000 }
h2 { font-family: Arial, Verdana, sans-serif; font-size: 20px; color: #000000 }
h3 { font-family: Arial, Verdana, sans-serif; font-size: 16px; color: #000000 }
h4 { font-family: Arial, Verdana, sans-serif; font-size: 14px; color: #000000 }

td, th  {
        text-align: left;
        padding: 4px;
            }

</style>

</head>
<body>
EOF

}


function fill_html {

echo "<h1 id="$1">Linux Systeme in der Stage <b>$1</b></h1>"

description=$(mysql --user=itzsdbuser --password=redhat --batch --silent --raw -e "SELECT DISTINCT description_cf from assets WHERE stage = '$1' ORDER BY description_cf;" itzsdb | sed -e 's/\s/xx/g')

counter=0

echo "<table>"
echo "<tr> <th>Anwendung</th> <th>Hosts</th> </tr>"
for i in $description
do

        desc=$(echo $i | sed -e 's/xx/ /g')
        hosts=$(mysql --user=itzsdbuser --password=redhat --batch --silent --raw -e "SELECT dns FROM assets WHERE description_cf = '$desc' AND stage = '$1';" itzsdb)

        host_count=$(echo $hosts | wc -w)
        host_count=$(($host_count + 1))


        if [ $(($counter % 2)) -eq 0 ]; then

                echo "<tr bgcolor="#ffffff"> <td rowspan="$host_count">$desc</td> </tr>"

                for x in $hosts
                do
                        echo "<tr bgcolor="#ffffff"> <td>$x</td> </tr>"
                done

        else

                echo "<tr> <td rowspan="$host_count">$desc</td> </tr>"

                for x in $hosts
                do
                        echo "<tr> <td>$x</td> </tr>"
                done
        fi

        counter=$(($counter + 1))

done
echo "</table>"

echo "<br>"
echo "<br>"
echo "<br>"

}

function close_html {

echo "<h4>Erstellungsdatum: $(date +%Y-%m-%d)</h4>"
echo "</body>"
echo "</html>"

}

prepare_html
fill_html PROD
fill_html QA
fill_html DEV
close_html