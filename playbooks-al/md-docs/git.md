## Allgemeines

Git \[ɡɪt\] ist eine freie Software zur
verteilten Versionsverwaltung von Dateien, die durch Linus Torvalds
initiiert wurde.  

GitLab ist eine Webanwendung zur Versionsverwaltung für Softwareprojekte
auf Basis von git.  
Sie bietet diverse Management und Bug-Tracking-Funktionalitäten, sowie
mit GitLab CI ein System zur kontinuierlichen Integration. GitLab ist in
der Programmiersprache Ruby entwickelt.  

Wird als "Source Control Manager" (SCM) in Verbindung mit [Ansible
Tower](Ansible_Tower "wikilink") eingesetzt.  

Der Server ist an IDM ([IPA](IPA "wikilink")) angebunden und ermöglicht
so einen gewohnten LDAP Login.  


## Verwendung von Git

Um mit Git arbeiten zu können, benötigt man den "Git Client" auf seinem
[Adminclient](Adminclient "wikilink").

    yum install git

Normalerweise legt man sich ein Projekt, z.B. in der WebUI
(https://git.ux.int.alte-leipziger.de), an.  
Initiale "things to do" werden dann auf der Projekt Startseite angezeigt
(Command line instructions).  
Man sollte als erstes seinen ssh-key in seinen "User Settings"
eintragen. Damit ist ein clone/pull/push ohne Passwortabfrage möglich.  
Den Anweisungen unter

    User Settings | SSH Keys

folgen um den ssh public key hinzuzufügen.

Man kann ein Projekt in der WebUI oder auf seinem Adminclient
bearbeiten. Dafür muss man es wie folgt beschrieben clonen.  

    git clone git@git.ux.int.alte-leipziger.de:onahlen/test01.git

Durch den Clone wurde nun ein Verzeichnis mit dem Projektnamen angelegt
(im aktuellen Verzeichnis\!).

    cd test01

 Dateien im Projektverzeichnis erstellen oder vorhandene Scripte aus
einem anderen Verzeichnis in das Projektverzeichnis kopieren.  
Anschließend mit dem 'add' Command dem Projekt hinzufügen. Wenn man
vorhandene Dateien nur ändert, reicht ein 'push'. Man muss sie dann
nicht erneut hinzufügen.

    git add _filename_

Alle Dateien (inkl. neu erstelltem Verzeichnis) dem Projekt hinzufügen

    git add .

Lokale Änderungen commiten, um sie später wieder ins Projekt
hochzuladen.

    git commit -a

um ein Neuaufsetzen für jBoss und initial-config von lxautotest zu
unterbinden:

    git commit -m "Aenderung bzw. comment"

Lokale Änderungen ins Projekt hochladen.  
Beim **ersten** push:

    git push -u origin master

Ab dem zweiten push kann man die Parameter weglassen:

    git push


Neueste Version (eines bereits geklonten Projektes) holen:

    cd test01
    
    git pull

  



## Update GitLab

Auf der GitLab Website ist immer die aktuelle Doku zum updaten auf die
entsprechende Version zu finden. WICHTIG immer Infos zum Update
durchlesen, manchmal gibt es haendische Tasks\!

### gitlab-runner

Nach dem letzen Update war der Gitlab-runner Service nicht mehr
vorhanden. Die Konfiguration und der User waren noch da, der Service
muss aber neu installiert werden. Zum testen ob die Runner noch
funktionieren, Projekt cloudforms-ruby anpassen. Dort wird eine CI
Pipeline zum CloudForms verwendet, wenn die Runner nicht installiert
sind, kommt dort die Meldunge "job stuck".

    gitlab-runner start

Wenn folgende Meldung kommt:

    FATAL: Failed to restart gitlab-runner: "systemctl" failed: exit status 5, Failed to restart gitlab-runner.service: Unit not found.

Dann muss der Service neu installiert werden:

    gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

Anschließend wieder starten.

## Good to know

### gitlab-ctl

Nach Änderungen an der /etc/gitlab/gitlab.rb:

    gitlab-ctl reconfigure

Status ansehen:

    gitlab-ctl status

GitLab neu starten:

    gitlab-ctl restart

### GIT deploy keys:

Nur als Admin ssh keys deployen. Dann kann man sie pro Projekt
auswählen.  
NICHT im Projekt generieren \!\!  

### gitlab-runner

Der service gitlab-runner.service muss laufen \!  
Ggf nach einem Update des Pakets gitlab-runner-1x.x.x-x.x86\_64
folgenden Befehl erneu ausführen:

    systemctl enable gitlab-runner.service

### git clone von github.com

Da immer wieder einige Dinge nur auf github.com verfügbar sind und das
Kopieren von vielen Files über den Windows-PC sehr aufwändig ist, ab
sofort folgende Lösung:  
\* auf uxkatello läuft SQUID als Proxy, dieser ermöglicht dem Host
lxbuild1 den Zugriff auf github.com über https

  - in /etc/profile.d/set\_https\_proxy.sh wird auf lxbuild1 für jeden
    Benutzer die Umgebungsvariable https\_proxy gesetzt
  - NUR auf lxbuild1 kann jetzt mit git repositories auf github.com
    gearbeitet werden, also z.B. git clone <https://github.com/project>

  

### Line of Codes über alle unsere Gitlab Projekte

Im Rahmen einer Präsentation stand ich vor der Fragestellung wie viele
"Zeilen Code" wir eigentlich bereits in unser GIT-Repository gebracht
haben. Nicht ganz einfach, daher hier was ich alles dazu unternehmen
musste:

    yum install cloc jq

dann ein cloc-git im Home anlegen

    #!/usr/bin/env bash
    git clone --depth 1 "$1" temp-linecount-repo &&
      printf "('temp-linecount-repo' will be deleted automatically)\n\n\n" &&
      cloc temp-linecount-repo &&
      rm -rf temp-linecount-repo

dann

    chmod u+x cloc-git

Jetzt muss man sich seinen "private\_token" aus der GitLab Web-UI
heraussuchen um einen authentifizierten Zugriff auf die API zu haben,
alle unsere Projects im GitLab sind ja "private". Damit kann man dann
folgende Abfrage senden:

    curl -k "https://git.ux.int.alte-leipziger.de/api/v4/projects?private_token=9gunZZZZZZZZZZZZZZZ"

Damit man das auch halbwegs lesen kann, kommt jq, ein JSON Commandline
Interpreter, zum Einsatz:

    curl -k "https://git.ux.int.alte-leipziger.de/api/v4/projects?private_token=9gunZZZZZZZZZZZZZZZ" | jq '.'
    [
      {
        "id": 74,
        "description": "",
        "default_branch": "master",
        "tag_list": [],
        "ssh_url_to_repo": "git@git.ux.int.alte-leipziger.de:unix-team/addressware-base-playbook.git",
        "http_url_to_repo": "https://git.ux.int.alte-leipziger.de/unix-team/addressware-base-playbook.git",
        "web_url": "https://git.ux.int.alte-leipziger.de/unix-team/addressware-base-playbook",
        "name": "addressware-base-playbook",
        "name_with_namespace": "unix-team / addressware-base-playbook",
        "path": "addressware-base-playbook",
        "path_with_namespace": "unix-team/addressware-base-playbook",
        "star_count": 0,
        "forks_count": 0,
        "created_at": "2018-02-08T10:59:55.411Z",
        "last_activity_at": "2018-02-09T10:05:15.711Z",

  


Immer noch eine Menge Output, uns interessiert ja nur die
ssh\_url\_to\_repo von allen unseren Projekten, daher können wir jetzt
mit jq entsprechend filtern:

    curl -k "https://git.ux.int.alte-leipziger.de/api/v4/projects?private_token=9gunZZZZZZZZZZZZZZZ" | jq -r '.[].ssh_url_to_repo'
    git@git.ux.int.alte-leipziger.de:unix-team/addressware-base-playbook.git
    git@git.ux.int.alte-leipziger.de:unix-team/ansible-bluecat-module.git
    git@git.ux.int.alte-leipziger.de:twinter/scripts.git
    git@git.ux.int.alte-leipziger.de:unix-team/aide.git
    git@git.ux.int.alte-leipziger.de:unix-team/awx-facts-playbooks.git
    git@git.ux.int.alte-leipziger.de:unix-team/pub-downloader-satellite.git
    git@git.ux.int.alte-leipziger.de:unix-team/ina-update-playbook.git
    git@git.ux.int.alte-leipziger.de:agogala/magento-poc.git
    git@git.ux.int.alte-leipziger.de:unix-team/cognos-systempatching-playbook.git
    git@git.ux.int.alte-leipziger.de:unix-team/patch-yum-adhoc-playbooks.git
    git@git.ux.int.alte-leipziger.de:unix-team/patch-inventories.git
    git@git.ux.int.alte-leipziger.de:unix-team/sap-systempatching-playbook.git
    git@git.ux.int.alte-leipziger.de:unix-team/openscap-satellite.git
    git@git.ux.int.alte-leipziger.de:unix-team/initial-config-playbook-dmz.git
    git@git.ux.int.alte-leipziger.de:agogala/jboss-eap-quickfix.git
    git@git.ux.int.alte-leipziger.de:unix-team/cloudforms-itzsdb.git
    git@git.ux.int.alte-leipziger.de:unix-team/dwh-base-playbooks.git
    git@git.ux.int.alte-leipziger.de:unix-team/upstream-solr-playbook.git
    git@git.ux.int.alte-leipziger.de:unix-team/jboss-eap-zeb-playbooks.git
    git@git.ux.int.alte-leipziger.de:unix-team/upstream-tomcat-base-playbooks.git

Diesen Output kann man dann in eine liste.txt schreiben und mit cloc-git
eine Auswertung fahren:

    for i in $(cat liste.txt); do ./cloc-git $i; done
    
    Cloning into 'temp-linecount-repo'...
    remote: Counting objects: 24, done.
    remote: Compressing objects: 100% (20/20), done.
    remote: Total 24 (delta 2), reused 13 (delta 0)
    Receiving objects: 100% (24/24), 7.29 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (2/2), done.
    ('temp-linecount-repo' will be deleted automatically)


​    
​          16 text files.
​          16 unique files.
​           3 files ignored.
​    -------------------------------------------------------------------------------
​    Language                     files          blank        comment           lcode
​    -------------------------------------------------------------------------------
​    YAML                            11             39              8            169
​    Bourne Shell                     1              4              2              6
​    INI                              1              0              0              2
​    Markdown                         1              0              0              1
​    -------------------------------------------------------------------------------
​    SUM:                            14             43             10            178
​    -------------------------------------------------------------------------------

## Git SpiderLab (owasp-modsecurity-crs)

`git log --pretty=oneline` - letzte Änderungen sehen und deren ID

`git reset --hard 3d96553111e5c59c09d5d59961c6c1e21bbd52b7` - reset auf
diese ID

`git checkout -f` - erneuter Sync mit dem online repo (falls was
gelöscht wurde)

`git remote update / git fetch --all`

`git ls-files` - listet die relevanten Files eines repos, wenn man im
Repo selbst steht

` git clone  `<https://github.com/SpiderLabs/owasp-modsecurity-crs>` 
/root/inSign-downloads/owasp-modsecurity-crs ` - das online repo clonen
(eine lokale Kopie anlegen)

`git remote -v` - welche repos werden getrackt, verbose

`git diff origin` - diff zwischen upstream und local

    #git remote -v update
    Fetching origin
    remote: Counting objects: 18, done.
    remote: Total 18 (delta 13), reused 13 (delta 13), pack-reused 5
    Unpacking objects: 100% (18/18), done.
    From https://github.com/SpiderLabs/owasp-modsecurity-crs
       735c0f0..6ffc7fd  v3.0.0-rc1 -> origin/v3.0.0-rc1

man pages: man git-remote - ist zB. die man page zum Befehl "git remote"

## Weblinks

  - [Das deutsche Git-Buch](http://gitbu.ch/)
  - [GitLab Docs](https://docs.gitlab.com/)
  - [git - der einfach
    Einstieg](https://rogerdudler.github.io/git-guide/index.de.html)
  - [ProGit](https://git-scm.com/book/en/v2)
  - [ProGit - Deutsch](https://github.com/progit/progit/tree/master/de)
  - [Getting Git Right](https://www.atlassian.com/git)
