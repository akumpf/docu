## Deployment

Make sure you are in the correct working directory while executen

Purging an existing cluster:

    ansible-playbook -i hosts /usr/share/ansible/openshift-ansible/playbooks/adhoc/uninstall.yml && ansible-playbook -i hosts /usr/share/ansible/openshift-ansible/playbooks/adhoc/uninstall_docker.yml

Prerequisites:

    ansible-playbook -i hosts /usr/share/ansible/openshift-ansible/playbooks/prerequisites.yml

Install:

    ansible-playbook -i hosts /usr/share/ansible/openshift-ansible/playbooks/deploy_cluster.yml

### LDAP integration

Copy LDAPSyncConfig.yml from the Git repo to the master

Execute LDAP Group sync by executing:

    oc adm groups sync --sync-config=./LDAPSyncConfig.yml --confirm

To add OCP roles to imported groups:

    oc adm policy add-cluster-role-to-group cluster-admin Administrators

### ECS integration

ECS certificates are located in:

    /home/ocpadm-test/openshift-neu/sandbox/cert/ecs

Create CA bundle:

    cat /home/ocpadm-test/openshift-neu/sandbox/cert/ecs/ecs-s3-ocp.cer > registry-ca-bundle.crt
    cat /home/ocpadm-test/openshift-neu/sandbox/cert/konzern-ca.crt >> registry-ca-bundle.crt
    cat /home/ocpadm-test/openshift-neu/sandbox/cert/KONZERN-ROOT.crt >> registry-ca-bundle.crt

Copy the CA Cert onto one Master into /tmp:

    registry-ca-bundle.crt

Execute the fix\_s3.yml playbook on the bastion node to create a secret
in the deployment config and mount it into the pods:

    ansible-playbook -i /home/ocpadm-test/openshift-neu/sandbox/hosts /home/ocpadm-test/openshift-neu/playbooks/fix_s3.yml

If the playbook fails, it might uses the wrong project. Switch to
project default first

### Download Images from Red Hat and upload to registry

Pull an image from registry.redhat.io:

    docker pull registry.redhat.io/rhscl/mariadb-103-rhel7

Tag the downloaded image to the openshift registry:

    docker tag registry.redhat.io/rhscl/mariadb-103-rhel7 docker-registry.default.svc:5000/PROJEKTNAME/mariadb

Copy Login Command from OCP WebUI -\> Username -\> Copy Login Command

Extract the token out of the login command:

    oc login https://lx7t0028.test.ux.int.alte-leipziger.de:443 --token=XGzz68AHW2p_9bNwCqMoUsN9-WvjQ5Fpty6iwpYMVbs

Login into the Registry service from openshift with the extracted token:

    docker login -u twinter-test -p XGzz68AHW2p_9bNwCqMoUsN9-WvjQ5Fpty6iwpYMVbs docker-registry.default.svc:5000

Push the tagged image to the openshift registry:

    docker push docker-registry.default.svc:5000/PROJEKTNAME/mariadb

## Useful stuff

Just random stuff I tried and documented

### Pull public image and push it into internal registry

    docker pull registry.redhat.io/rhel7-minimal

    docker tag registry.redhat.io/rhel7-minimal docker-registry.default.svc:5000/<openshift-project>/rhel7:latest

Username is the standard IPA user and password can be copied in the OCP
WebUI.

    docker login docker-registry.default.svc:5000

    docker push docker-registry.default.svc:5000/<openshift-project>/rhel7

### Create networkpolicy

To block for example everything create a YAML file:

    [ocpadm-test@lx7t0033 ~]$ cat default-deny.yml 
    kind: NetworkPolicy
    apiVersion: extensions/v1beta1
    metadata:
      name: default-deny
    name: default-deny
    spec:
    podSelector:

Go into the desired project:

    oc project <project>

See if there are any existing policies:

    oc get networkpolicy

Create policy:

    oc create -f default-deny.yml

To delete the policy:

    oc delete networkpolicy/default-deny

## Certificates

We have several OCP related certificates:

  - Public KONZERN CA signed application certificate (wildcard
    certificate on F5, f.e. \*.ocp.al-h-konzern.de)
  - Public KONZERN CA signed webui certificate (certificate on F5, f.e.
    ocp-console.ux.int.alte-leipziger.de)
  - Cluster internal CA certificate (cert used in OCP config). This
    certificate will be used to sign the following certs:
      - All cluster internal certs for masters, infras etc (see
        /etc/origin/master)
      - ECS S3 certificate for ecs-s3-ocp.ux.int.alte-leipziger.de

### Public KONZERN CA signed application certificate

#### Creation

Created on lxtower1 with following config:

  - openssl-IT-zs-ls.ocpprod.conf
  - openssl-IT-zs-ls.ocptest.conf

Private key (no password):

    openssl genrsa -out /root/ssl_cert/openshift-prod.pem 4096

    openssl genrsa -out /root/ssl_cert/openshift-test.pem 4096

Certificate:

    openssl req -new -key /root/ssl_cert/openshift-prod.pem -out /root/ssl_cert/openshift_prod_cert.csr -config openssl-IT-zs-ls.ocpprod.conf -batch

    openssl req -new -key /root/ssl_cert/openshift-test.pem -out /root/ssl_cert/openshift_test_cert.csr -config openssl-IT-zs-ls.ocptest.conf -batch

#### Info Production Cert

Validity:

    Not After : Sep  4 10:10:00 2021 GMT

Subject:

    C=DE, ST=Hessen, L=Oberursel, O=Alte Leipziger Hallesche Konzern, OU=IT-zs-ls, CN=*.ocp.al-h-konzern.de

SAN:

    DNS:*.ocp.al-h-konzern.de

#### Info Test Cert

Validity:

    Not After : Sep  4 10:00:00 2021 GMT

Subject:

    C=DE, ST=Hessen, L=Oberursel, O=Alte Leipziger Hallesche Konzern, OU=IT-zs-ls, CN=*.ocp.test.ux.int.alte-leipziger.de

SAN:

    DNS:*.ocp.test.ux.int.alte-leipziger.de

### Public KONZERN CA signed webui certificate

#### Creation

Created on lxtower1 with following config:

  - openssl-IT-zs-ls.ocptestmaster.conf

Private key (no pass):

    openssl genrsa -out /root/ssl_cert/openshift/openshift-testmaster.pem 4096

Certificate:

    openssl req -new -key /root/ssl_cert/openshift/openshift-testmaster.pem -out /root/ssl_cert/openshift/openshift_test_master_cert.csr -config openssl-IT-zs-ls.ocptestmaster.conf -batch

#### Info Production Cert \!ToDo\!

Validity:

    Not After : Sep  9 09:20:00 2021 GMT

Subject:

    C=DE, ST=Hessen, L=Oberursel, O=Alte Leipziger Hallesche Konzern, OU=IT-zs-ls, CN=ocp-console.test.ux.int.alte-leipziger.de

SAN:

    DNS:ocp-console.test.ux.int.alte-leipziger.de

#### Info Test Cert

Validity:

    Not After : Sep  9 09:20:00 2021 GMT

Subject:

    C=DE, ST=Hessen, L=Oberursel, O=Alte Leipziger Hallesche Konzern, OU=IT-zs-ls, CN=ocp-console.test.ux.int.alte-leipziger.de

SAN:

    DNS:ocp-console.test.ux.int.alte-leipziger.de

### Cluster internal CA certificate

We have one CA Cert for both OCP cluster (Test & Prod) because we only
can use one certificate for the S3 interface on the ECS. Not signed by
Konzern CA because of wildcard \*ux.int.alte-leipziger.de. If someone
steals this CA cert it is not trusted by AL clients

#### Creation

Created on lxtower1 with following config:

  - openssl-IT-zs-ls.ocp-ca.conf

Private key (see keepass for passwords):

    openssl genrsa -des3 -out OCProotCA.key 4096

Certficate:

    openssl req -x509 -new -nodes -key OCProotCA.key -sha256 -days 3650 -out OCProotCA.crt -config ../../openssl-IT-zs-ls.ocp-ca.conf

Look at certificate:

    openssl x509 -in OCProotCA.crt -text

#### Info Cert

Validity:

    Not After : Sep 10 09:39:34 2029 GMT

Subject:

    C=DE, ST=Hessen, L=Oberursel, O=Alte Leipziger Hallesche Konzern, OU=IT-zs-ls, CN=*.ux.int.alte-leipziger.de

SAN:

    DNS:*.ux.int.alte-leipziger.de

### Cluster internal certs

### ECS S3 certificate

#### Creation

Created on ecs-obu-01.1 with following config:

  - request.conf

Private key (see keepass):

    openssl genrsa -des3 -out ecs-s3-ocp.key 2048

Certificate signing request:

    openssl req -new -key ecs-s3-ocp.key -config request.conf -out ecs-s3-ocp.csr -days 3650

Certificate signing:

    openssl x509 -req -in ecs-s3-ocp.csr -CA OCProotCA.crt -CAkey OCProotCA.key -CAcreateserial -out ecs-s3-ocp.crt -days 3650 -sha256

#### Info Cert

Validity:

    Not After : Sep 10 09:52:48 2029 GMT

Subject:

    C=DE, ST=Hessen, L=Oberursel, O=Alte Leipziger Hallesche Konzern, OU=IT-zs-ls, CN=ecs-s3-ocp.ux.int.alte-leipziger.de

SAN:

    DNS:ecs-s3-ocp.ux.int.alte-leipziger.de, IP Address:10.1.105.10, IP Address:10.1.105.11, IP Address:10.1.105.12, IP Address:10.1.105.13, IP Address:10.1.105.14, IP Address:10.1.105.20, IP Address:10.1.105.21, IP Address:10.1.105.22, IP Address:10.1.105.23, IP Address:10.1.105.24

#### Update Cert on ECS

Steps based on ECS Administration Guide:

Log into ecs-obu-01-1:

    ssh admin@10.1.111.100 && cd certs

Get authentication token from ECS API. When executed type in the
password of the root user:

    export TOKEN=`curl -s -k -v -u root https://$(hostname -i):4443/login 2>&1 | grep X-SDS-AUTH-TOKEN | awk '{print $2, $3}'`

Upload certificate and private key to ECS S3 data enpoint:

    curl -svk -H "$TOKEN" -H "Content-type: application/xml" -H "X-EMC-REST-CLIENT: TRUE" -X PUT -d "<rotate_keycertchain><key_and_certificate><private_key>`cat ecs-s3-ocp-nopass.key`</private_key><certificate_chain>`cat ecs-s3-ocp.crt`</certificate_chain></key_and_certificate></rotate_keycertchain>" https://10.1.111.100:4443/object-cert/keystore

Restart dataheadsvc:

    sudo kill `pidof dataheadsvc`
