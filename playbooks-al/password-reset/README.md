resets the password of a local user (i.e. on exadata hosts).  
standalone playbook because of future use of different departments (i.e. oracle-admins).  
ansible survey requires new password. username is taken from ansible-tower login !  

TEMPLATE: exa-password-reset  