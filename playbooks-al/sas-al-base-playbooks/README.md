Playbook fuer die Installation von SAS AL

PB bereitet den Server fuer die SAS Installation vor.
Die eigenetliche SAS Installation erfolgt dann manuell. Stand 05.04.2018

- SAS Konfiguration fuer LDAP Anbindung
  Vorbereitungen durch das Playbook VOR der SAS Installation
  - sasauth nach /etc/pam.d kopiert.
  - Verzeichnis /app/sas/sasauth fuer die Log Dateien angelegt.
  - Link /usr/lib64/libgssapi_krb5.so auf /usr/lib64/libgssapi_krb5.so.2 angelegt.
  Manuelle Arbeiten NACH der SAS Installation
  - sasauth.conf aus dem install/templates Ordner nach /app/sas/sashome/SASFoundation/9.4/utilities/bin kopieren
  - Die IPA SAS User in die /etc/group eintragen
  - SAS durchstarten

Noch offen:

----

Erledigt:
- Oracle Java aus dem Playbook entfernt. SAS bringt eigenes Java mit.
- Windows cifs mount konfiguriert
  - Verzeichnis /app/sas/sas-ag-share anlegen
  - /etc/fstab anpassen
  - cifs credentials unter /etc ablegen
- /app/sas/temp Ordner und Share entfernt. Wird nicht benoetigt.
- User ysrvft1, ysrvsas und ysrvmeta anlegen. Fuer UC4, Metadaten
- Umstellung auf LDAP Accounts
- Die Berechtigungen auf die sas-dev, sas-work und sas-prod Laufwerke setzen. chmod g+s
- sasauth konfigurieren
- PAM Modul fuer sasauth kopieren
- Link auf krb Lib setzen
- Accounts in sas Gruppe (manuell)
- Anlegen einer App Disk. Groesse 120 GB
- Zusaetzliche rpm Pakete fuer die DB2 Client installation
- Anlegen der beiden User sas und sassrv. Beide in der sas Gruppe.
- Zusaetzliche Pakete fuer die SAS Installation
  xorg-x11-server-Xorg libXtst xorg-x11-xauth numactl glibc.i686 libpng libXrender fontconfig apr ksh libXp
- Zusaetzlicher Channel rhel-7-optional fuer compat-libstdc++-33
- Oracle Java SDK installieren
- Lizenz Ordner unter /app/sas anlegen. Eventuell den Link /etc/opt/vmware/vfabric anlegen
- Firewall Ports: 9621/tcp 
- ulimit Werte setzen. Siehe checklist.pdf Seite 14
- Installation des DB2 Clients wenn moeglich. Nur teilweise moeglich z.B. Verzeichnisse anlegen, auspacken.
- umask wieder auf 0022 gesetzt.
