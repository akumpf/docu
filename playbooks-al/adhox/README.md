This repository should contain everything you need to send
out ansible adhoc commands to our environment.

Details will follow:

- Clone this repository to a working dir in your home

- there is a ansible.cfg with useful settings, so run all ansible commands
  from the root-directory of this project in your working dir

- to manage RHEL 6 systems, go to satellite.ux:

	ansible -i inventory/sat5 all --list-hosts (list ALL hosts)

	ansible -i inventory/sat5 localhost -m debug -a 'var=groups' (trick to view all groups)

	ansible -i inventory/sat5 QA --list-hosts (list all hosts in group QA)

	ansible -i inventory/sat5 QA -m ping

	ansible -i inventory/sat5 QA -m ping -b -K (do something as root)

- to manage RHEL 7 systems, should work from adminclient:

	ansible -i inventory/sat6 all --list-hosts

	ansible -i inventory/sat6 localhost -m debug -a 'var=groups'

	ansible -i inventory/sat6 foreman_lifecycle_environment_dev -m ping

IMPORTANT:

Getting the inventory from Sat5 and Sat6 takes a loooong time, be patient.
The results are cached for 10 minutes, so only the first run is slow.


