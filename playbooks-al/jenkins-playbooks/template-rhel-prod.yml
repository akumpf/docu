- name: Call to hammer commands
  hosts: lxsat.ux.int.alte-leipziger.de

  tasks:
    - include_vars: main.yml
    - name: Obtain SSO token with using username/password credentials
      ovirt_auth:
       url: https://rhvmp.ux.int.alte-leipziger.de/ovirt-engine/api
       username: rhv-admin-jenkins@ux.int.alte-leipziger.de
       password: "{{ rhv_admin_jenkins_intranet_password }}"
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - set_fact: rhel_version="{{ hostvars['template-rhel-prod.ux.int.alte-leipziger.de'].foreman.medium_name[-3:] }}"

    - name: Remove the system if exists
      ovirt_vm:
        auth: "{{ ovirt_auth }}"
        name: template-rhel-prod
        state: absent
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - set_fact: disk_id="{{ ansible_date_time.epoch }}"

    - name: Add the template disk
      ovirt_disk:
        auth: "{{ ovirt_auth }}"
        name: "tpl_{{ disk_id }}_Disk0"
        #vm_name: lxautotest1
        interface: virtio_scsi
        storage_domain: VPLEX-Storage-Prod-01
        bootable: True
        size: 20GiB
        format: raw
        state: present
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - name: Enable build mode
      command: hammer host update --name template-rhel-prod.ux.int.alte-leipziger.de --build=true

    - name: Add the system to convert to template
      ovirt_vm:
        auth: "{{ ovirt_auth }}"
        name: template-rhel-prod
        state: present
        cluster: UCS-Prod
        high_availability: yes
        memory: 16GiB
        cpu_cores: 2
        boot_devices: ['hd','network']
        cpu_sockets: 1
        type: server
        operating_system: rhel_7x64
        timeout: 300
        disks:
          - name: "tpl_{{ disk_id }}_Disk0"
            bootable: True
            interface: virtio_scsi
        nics:
          - name: nic1
            mac_address: 00:1a:4a:17:05:e6
            profile_name: AL_VLAN211
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - name: Start the system
      ovirt_vm:
        auth: "{{ ovirt_auth }}"
        name: template-rhel-prod 
        state: running 
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - name: Wait till system is reachable
      wait_for:
         host: template-rhel-prod.ux.int.alte-leipziger.de
         port: 22
         delay: 10 
         timeout: 900
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - name: Wait till system is NOT reachable
      wait_for:
         host: template-rhel-prod.ux.int.alte-leipziger.de
         port: 22
         delay: 10 
         state: stopped
         timeout: 900
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de


    - name: Stop the system
      ovirt_vm:
        auth: "{{ ovirt_auth }}"
        name: template-rhel-prod 
        state: stopped
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - name: Remove the template
      ovirt_template:
        auth: "{{ ovirt_auth }}"
        cluster: UCS-Prod
        name: "template-rhel-prod-{{ rhel_version }}"
        state: absent
        timeout: 600
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de


    - name: Convert VM to template
      ovirt_template:
        auth: "{{ ovirt_auth }}"
        cluster: UCS-Prod
        name: "template-rhel-prod-{{ rhel_version }}"
        vm: template-rhel-prod
        timeout: 600
        description: "Template cread by Ansible on {{ ansible_date_time.date }} {{ansible_date_time.time }}"
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de

    - name: Remove the system if exists
      ovirt_vms:
        auth: "{{ ovirt_auth }}"
        name: template-rhel-prod
        state: absent
        timeout: 600
      delegate_to: lxrhvm1.ux.int.alte-leipziger.de
    - block:
      - name: CF | Get auth token
        delegate_to: localhost
        uri:
         url: https://cfweb.ux.int.alte-leipziger.de/api/auth
         method: GET 
         user: "{{ cf_user }}"
         password: "{{ cf_password }}"
         validate_certs: False
        register: auth_token
      - set_fact: token="{{ auth_token.json.auth_token }}"
  
      - name: CF | Get Template
        delegate_to: localhost
        uri:
         url: "https://cfweb.ux.int.alte-leipziger.de/api/templates?expand=resources&attributes=name,power_state&filter[]=name=template-rhel-prod-{{ rhel_version }}&filter[]=power_state=never"
         method: GET
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
        register: templates
  
      - name: CF | Get the service template "RHEL {{ rhel_version }} in PROD"
        delegate_to: localhost
        uri:
         url: "https://cfweb.ux.int.alte-leipziger.de/api/service_templates?expand=resources&filter[]=name=RHEL%20{{ rhel_version|replace('_', '.') }}%20in%20Production"
         method: GET
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
        register: st_href
  
      - name: CF | GET Service template 
        delegate_to: localhost
        uri:
         url: "{{ st_href.json.resources[0].href }}"
         method: GET
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
        register: st
  
      - set_fact: template_id="{{ templates.json.resources[0].id }}"
      - set_fact:
          config_info: >
            {{ st.json.config_info|combine({ 'src_vm_id': [template_id, 'template-rhel-prod-' + rhel_version ] })  }}
  
      - set_fact: 
          config_info_good: {}
  
      # Workaround to remove the key miq_request_dialog_name
      - set_fact:
         config_info_good: "{{ config_info_good | combine({item.key : item.value})}}"
        when: "{{ item.key not in ['miq_request_dialog_name'] }}"
        with_dict: "{{ config_info }}"
  
  
      - name: CF | Update Service template with the new RHV template id
        delegate_to: localhost
        uri:
         url: "{{ st_href.json.resources[0].href }}"
         method: POST
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
         body: |
          { "action": "edit", "resource": {  "config_info": {{ config_info_good | to_json }} } }
         body_format: "json"
        register: st
        retries: 5
        delay: 3


  
      - name: CF | AUTOTEST | Get the service template "RHEL {{ rhel_version }} in PROD"
        delegate_to: localhost
        uri:
         url: "https://cfweb.ux.int.alte-leipziger.de/api/service_templates?expand=resources&filter[]=name=AUTOTEST%20RHEL%20{{ rhel_version|replace('_', '.') }}%20in%20Production"
         method: GET
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
        register: st_href
  
      - name: CF | AUTOTEST | GET Service template 
        delegate_to: localhost
        uri:
         url: "{{ st_href.json.resources[0].href }}"
         method: GET
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
        register: st
  
      - set_fact: template_id="{{ templates.json.resources[0].id }}"
      - set_fact:
          config_info: >
            {{ st.json.config_info|combine({ 'src_vm_id': [template_id, 'template-rhel-prod-' + rhel_version ] })  }}
  
      - set_fact: 
          config_info_good: {}
  
      # Workaround to remove the key miq_request_dialog_name
      - set_fact:
         config_info_good: "{{ config_info_good | combine({item.key : item.value})}}"
        when: "{{ item.key not in ['miq_request_dialog_name'] }}"
        with_dict: "{{ config_info }}"
  
  
      - name: CF | AUTOTEST | Update Service template with the new RHV template id
        delegate_to: localhost
        uri:
         url: "{{ st_href.json.resources[0].href }}"
         method: POST
         HEADER_X-Auth-token: "{{ token }}"
         validate_certs: False
         body: |
          { "action": "edit", "resource": {  "config_info": {{ config_info_good | to_json }} } }
         body_format: "json"
        register: st
        retries: 5
        delay: 3
   
  
      - name: CF | Delete auth token
        delegate_to: localhost
        uri:
         url: https://cfweb.ux.int.alte-leipziger.de/api/auth
         method: DELETE
         HEADER_X-Auth-token: "{{ token }}"
         user: "{{ cf_user }}"
         password: "{{ cf_password }}"
         validate_certs: False
         body_format: "json"
         status_code: 204
        register: auth_token
  
  
  
      - name: Revoke the oVirt SSO toekn
        delegate_to: localhost
        ovirt_auth:
          state: absent
          ovirt_auth: "{{ ovirt_auth }}"
      become: False
 
