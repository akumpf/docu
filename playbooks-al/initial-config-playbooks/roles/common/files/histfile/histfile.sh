# History per server in case the user has nfs directory
if [[ $HOME == "/nfs/home/"* ]]; then
	export HISTFILE="${HOME}/.bash_history.`hostname -s`"
fi

# Set historyfile format for all users 
export HISTTIMEFORMAT='%d.%m.%Y %T - '
