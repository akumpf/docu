#
# alh_postprovision.rb
#
# This CloudForms Automate Method is intended to run after provisioning a VM,
# when the VM is already available as an object inside CloudForms. It will do
# provider-independent tasks on the VM, for example
# - assigning tag values chosen in the service dialog to the VM
# - storing values from the service dialog in custom attributes
# - storing the VM name in the service name (work in progress)
#

# Get provisioning object
prov = $evm.root['miq_provision']

# Get the VM object
vm = prov.vm

# Custom attributes - these are just string values from the dialog
alh_mail_address    = prov.get_option(:dialog_mail_address)
alh_requester_name  = prov.get_option(:dialog_requester_name)

prov.vm.custom_set('AL-H Requester Name', alh_requester_name)
prov.vm.custom_set('AL-H Requester Mail', alh_mail_address)

# Tags - set the classifications from the service dialog

alh_linux_lifecycle = prov.get_option(:dialog_alh_linux_lifecycle_name)
alh_ae_umgebung     = prov.get_option(:dialog_alh_ae_umgebung)
alh_kundengruppe    = prov.get_option(:dialog_alh_kundengruppe)

$evm.log('info', "alh_linux_lifecycle = #{alh_linux_lifecycle}")
$evm.log('info', "alh_kundengruppe = #{alh_kundengruppe}")
$evm.log('info', "alh_ae_umgebung = #{alh_ae_umgebung}")

vm.tag_assign("alh_ae_umgebung/#{alh_ae_umgebung}")
vm.tag_assign("alh_linux_lifecycle/#{alh_linux_lifecycle}")
vm.tag_assign("alh_kundengruppe/#{alh_kundengruppe}")

# Code example for setting the service name to VM name(s), not used yet
# NOT TESTED
# 
# destination = $evm.root['service_template_provision_task'].destination
# vmnamelist = ''
# destination.vms.each.do |singlevm|
#   if vmnamelist == ''
#     vmnamelist = singlevm.name.to_s
#   else
#     vmnamelist = vmnamelist + ' ' + singlevm.name.to_s
#   end
#   $evm.log('info', "vmnamelist = #{vmnamelist}")
# end
# 
# destination.name = destination.name + ' (' + vmnamelist + ')'
#
# agonzalez: Force a refresh
ems = vm.ext_management_system if vm
ems.refresh
exit MIQ_OK
