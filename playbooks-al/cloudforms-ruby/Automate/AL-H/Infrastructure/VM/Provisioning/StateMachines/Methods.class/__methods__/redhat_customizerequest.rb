#
# redhat_customizerequest.rb CloudForms Automate Method
#
# This method customizes a provisioning request:
# - It adjusts memory/core resources based on t-shirt sized.
# - It overrides manual values from the dialog.
# - It sets the VLAN for the provider.
# - It sets the VM description based on a dialog field value.
#
# Remark: this works only for a RHV provider so far.
# ======= If another provider type is added, the script has to be adjusted to cover two providers.
#

# Get provisioning object
def log(level, msg, update_message = false)
  $evm.log(level, "#{msg}")
  $evm.root['miq_provision'].message = "#{msg}" if $evm.root['miq_provision'] && update_message
end


prov = $evm.root["miq_provision"]

$evm.log("info", "Provisioning ID:<#{prov.id}> Provision Request ID:<#{prov.miq_provision_request.id}> Provision Type: <#{prov.provision_type}>")

$evm.root.attributes.sort.each { |k,v| $evm.log("info", "EVM:Root attributes: #{v}: #{k}") }

# ------- Resources - t-Shirt sizes
request_hostname = prov.get_option(:vm_name)
$evm.log('info', "Request hostname: #{request_hostname}")

tshirtsize = prov.get_option(:dialog_tshirtsize)
vm_memory = prov.get_option(:dialog_vm_memory)
vm_cores = prov.get_option(:dialog_vm_cores)

case tshirtsize
when "M"
    prov.set_option(:vm_memory,4096)
    prov.set_option(:cores_per_socket,2)
    $evm.log("info", "T-Shirt Size Medium: 2 Cores, 4 GB RAM")
when "L"
    prov.set_option(:vm_memory,4096)
    prov.set_option(:cores_per_socket,4)
    $evm.log("info", "T-Shirt Size Large: 4 Cores, 4 GB RAM")
when "XL"
    prov.set_option(:vm_memory,8192) 
    prov.set_option(:cores_per_socket,4)
    $evm.log("info", "T-Shirt Size Extra Large: 4 Cores, 8 GB RAM")
else 
    $evm.log("warn", "Unkonwn T-Shirt Size!")
end

# ------- Resources - manual overrides

if vm_memory.to_i > 0
    prov.set_option(:vm_memory,vm_memory.to_i * 1024)
    $evm.log("info", "Memory Override: #{vm_memory}")
end
if vm_cores.to_i > 0
    prov.set_option(:cores_per_socket,vm_cores.to_i)
    $evm.log("info", "Cores Override: #{vm_cores}")
end

prov.set_option(:number_of_sockets, 1)

# ------- Set the VLAN
# This part is provider specific, naming on VMware by be different than on RHV

dialog_vlan = prov.get_option(:dialog_vm_vlan)

$evm.log("info", "VLAN list from Dialog: #{dialog_vlan}")

vlan_list={}

t = dialog_vlan.split('|')
t.each { |u|
    k,v=u.split('=')
    vlan_list[k]=v
}

$evm.log("info", "vlan_list: #{vlan_list}")
#OLD code CFME 5.8
#prov.set_option(:vlan, [vlan_list["RHV"], vlan_list["RHV"]])

#NEW Code CFME 5.10
# The format needs to be "#{vnic_profile_name} (#{network_name})"
selected_vlan = "#{vlan_list["RHV"]} (#{vlan_list["RHV"]})"
prov.set_vlan(selected_vlan)

log(:info, "Provisioning object <:vlan> updated with <#{selected_vlan}>", true)

# ------- Resources - set the description

alh_description = prov.get_option(:dialog_alh_description)
prov.set_option(:vm_description, alh_description)

# Enable for debugging
prov.attributes.sort.each { |k,v| $evm.log("info", "Prov attributes: #{v}: #{k}") }

exit MIQ_OK
