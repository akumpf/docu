#
# power_on_vm CloudForms Automate Method
#
# This method is intended to run in a provisioning context. It's only
# purpiose is, to start a VM. A useful extension would be, to wait until
# CloudForms reports the machine really as running, before exiting.
#

prov = $evm.root['miq_provision']
vm = prov.vm

$evm.log("info", "Powering on VM")

vm.start

exit MIQ_OK
