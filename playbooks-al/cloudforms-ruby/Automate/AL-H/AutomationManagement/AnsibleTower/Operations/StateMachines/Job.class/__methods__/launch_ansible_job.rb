#
# Description: Launch a Ansible Job Template and save the job id
#              in the state variables so we can use it when we
#              wait for the job to finish.
#

module ManageIQ
  module Automate
    module AutomationManagement
      module AnsibleTower
        module Operations
          module StateMachines
            module Job
              class LaunchAnsibleJob
                ANSIBLE_VAR_REGEX = Regexp.new(/(.*)=(.*$)/)
                ANSIBLE_DIALOG_VAR_REGEX = Regexp.new(/dialog_param_(.*)/)
                SCRIPT_CLASS = 'ManageIQ_Providers_AnsibleTower_AutomationManager_ConfigurationScript'.freeze
                JOB_CLASS = 'ManageIQ_Providers_AnsibleTower_AutomationManager_Job'.freeze
                MANAGER_CLASS = 'ManageIQ_Providers_AnsibleTower_AutomationManager'.freeze

                def initialize(handle = $evm)
                  @handle = handle
                end

                def main
                  run(job_template, target)
                end

                private

                def target
                  $evm.log("info", "Listing Root Object Attributes:")
                  $evm.root.attributes.sort.each { |k, v| $evm.log("info", "\t#{k}: #{v}") }
                  $evm.log("info", "===========================================")
                  # If are in provisioning state, then miq_provision value exists.
                  if $evm.root['miq_provision'] 
                    $evm.log("info", "Using the vm_fqdn variable")
                    vm_from_request
                  else
                    # Here should be on retirement OR in a button call
                    $evm.log("info", "Using the vm object")
                    vm = $evm.root['vm']
                    unless vm.hostnames.empty? # If the provisioning fails, then the ovirt-agent didn't work and we don't have hostnames
                        $evm.log("info", "Using vm.hostnames from the vm object")
                        vm.hostnames.first.presence if vm.hostnames
                    else # Last chance: using the name of the vm plus ".*" -> DANGEROUS?
                        $evm.log("info", "Without hostname, we have to use the vm.name")
                        vm.name + ".*"
                    end
                  end
                end

                def vm_from_request
                  #@handle.root["miq_provision"].try(:destination)
                  $evm.get_state_var('vm_fqdn').to_s
                end

                def ansible_vars_from_objects(object, ext_vars)
                  return ext_vars unless object
                  ansible_vars_from_objects(object.parent, object_vars(object, ext_vars))
                end

                def object_vars(object, ext_vars)
                  # We are traversing the list twice because the object.attributes is a DrbObject
                  # and when we use each_with_object on a DrbObject, it doesn't seem to update the
                  # hash. We are investigating that
                  key_list = object.attributes.keys.select { |k| k.start_with?('param', 'dialog_param') }
                  key_list.each_with_object(ext_vars) do |key, hash|
                    if key.start_with?('param')
                      match_data = ANSIBLE_VAR_REGEX.match(object[key])
                      hash[match_data[1].strip] ||= match_data[2] if match_data
                    else
                      match_data = ANSIBLE_DIALOG_VAR_REGEX.match(key)
                      hash[match_data[1]] = object[key] if match_data
                    end
                  end
                end

                def ansible_vars_from_options(ext_vars)
                  options = @handle.root["miq_provision"].try(:options) || {}
                  options.each_with_object(ext_vars) do |(key, value), hash|
                    match_data = ANSIBLE_DIALOG_VAR_REGEX.match(key.to_s)
                    hash[match_data[1]] = value if match_data
                  end
                end

                def var_search(obj, name)
                  return nil unless obj
                  obj.attributes.key?(name) ? obj.attributes[name] : var_search(obj.parent, name)
                end

                def job_template
                @handle.log(:info, "Entering def job_template")
                  job_template = var_search(@handle.object, 'job_template') ||
                                 job_template_by_id ||
                                 job_template_by_provider ||
                                 job_template_by_name

                  if job_template.nil?
                    raise "Job Template not specified"
                    @handle.log(:info, "Job Template not specified")
                  end
                  job_template
                end

                def job_template_name
                @handle.log(:info, "Entering def job_template_name")    
                  @job_template_name ||= var_search(@handle.object, 'job_template_name') ||
                                         var_search(@handle.object, 'dialog_job_template_name')
                  # Alberto replace template name with the correct environment
                  if @job_template_name == "infra-ENV-initial-config"
                      prov = $evm.root['miq_provision']
                      @job_template_name = @job_template_name.gsub("ENV",prov.get_option(:dialog_alh_linux_lifecycle_name).upcase)
                  end

                  # added by O.Nahlen. Get location for playbook decision retire VM.
                  if @job_template_name == "cloudforms-ENV-retirement-config"
                    #envtmp = target if target
                    vm = $evm.root['vm']
                    envtmp = vm.name
                    @handle.log(:info, "#{vm.name}")
                    if envtmp.include? "lx7t" 
                        @job_template_name = "cloudforms-testnetz-retirement-config"
                        @handle.log(:info, "call cloudforms-testnetz-retirement-config")
                    elsif envtmp.include? "lx7d"
                        @job_template_name = "cloudforms-DMZ-retirement-config"
                        @handle.log(:info, "call cloudforms-DMZ-retirement-config")
		    else
			@job_template_name = "cloudforms-Intranet-retirement-config"
                        @handle.log(:info, "call cloudforms-Intranet-retirement-config")
                    end
                  end


                  # added by O.Nahlen. Get location for playbook decision pre-initial-config
                  if @job_template_name == "infra-ENV-pre-initial-config"
                    envtmp = target if target
                    @handle.log(:info, envtmp)
                    if envtmp.include? "lx7d" 
                        @job_template_name = "infra-dmz-pre-initial-config"
                        @handle.log(:info, "call infra-dmz-pre-initial-config")
                    elsif envtmp.include? "lx7t" 
                        @job_template_name = "infra-testnetz-pre-initial-config"
                        @handle.log(:info, "call infra-testnetz-pre-initial-config")
		    else
			@job_template_name = "infra-pre-initial-config"
                        @handle.log(:info, "call infra-pre-initial-config")
                    end
                  end

                  # added by O.Nahlen. Get location for playbook decision to install AIDE
                  if @job_template_name == "infra-ENV-aide"
                    envtmp = target if target
                    @handle.log(:info, envtmp)
                    if envtmp.include? "lx7d" 
                        @job_template_name = "infra-DMZ-aide"
                        @handle.log(:info, "call infra-DMZ-aide playbook from tower")
                    elsif envtmp.include? "lx7t"
                        @job_template_name = "infra-testnetz-aide"
                        @handle.log(:info, "call infra-testnetz-aide playbook from tower")
		    else
			@job_template_name = "infra-intranet-aide"
                        @handle.log(:info, "call infra-intranet-aide playbook from tower")
                    end
                  end
                  
                  @handle.log(:info, "Job Template Name #{@job_template_name}")
                  job_template_name = @job_template_name
                  @job_template_name
                end

                def job_template_by_name
                  @handle.vmdb(SCRIPT_CLASS).where('lower(name) = ?', job_template_name.downcase).first if job_template_name
                end

                def job_template_by_id
                  job_template_id = var_search(@handle.object, 'job_template_id') ||
                                    var_search(@handle.object, 'dialog_job_template_id')
                  @handle.vmdb(SCRIPT_CLASS).where(:id => job_template_id).first if job_template_id
                end

                def job_template_by_provider
                  provider_name = var_search(@handle.object, 'ansible_tower_provider_name') ||
                                  var_search(@handle.object, 'dialog_ansible_tower_provider_name')
                  provider = @handle.vmdb(MANAGER_CLASS).where('lower(name) = ?', provider_name.downcase).first if provider_name
                  provider.configuration_scripts.detect { |s| s.name.casecmp(job_template_name).zero? } if provider && job_template_name
                end

                def extra_variables
                  result = ansible_vars_from_objects(@handle.object, {})
                  ansible_vars_from_options(result)
                end

                def run(job_template, target)
                  @handle.log(:info, "Processing Job Template #{job_template.name}")
                  args = {:extra_vars => extra_variables}
                  vm = $evm.root['vm']
                  target = 'lxbuild1*' if job_template.name == 'update-tower-inventory'
                  target = 'lxbuild1*' if job_template.name == 'cloudforms-itzsdb-initial'
                  #target = 'lxbuild1*' if job_template.name == 'cloudforms-ENV-retirement-config'
                  #target = 'lxbuild1*' if job_template.name == 'cloudforms-intranet-retirement-config'
                  target = 'lxbuild1*' if job_template.name == 'cloudforms-itzsdb-retire'
                  target = 'lxbuild1*' if job_template.name == 'cloudforms-itzsdb'
                  args[:limit] = target if target
		  # Extra logging twinter
                  @handle.log(:info, "About to start template: #{job_template.name}")
                  @handle.log(:info, "Job Arguments #{args}")

                  job = @handle.vmdb(JOB_CLASS).create_job(job_template, args)

                  @handle.log(:info, "Scheduled Job ID: #{job.id} Ansible Job ID: #{job.ems_ref}")
                  @handle.set_state_var(:ansible_job_id, job.id)
                end
              end
            end
          end
        end
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  ManageIQ::Automate::AutomationManagement::AnsibleTower::Operations::StateMachines::Job::LaunchAnsibleJob.new.main
end
