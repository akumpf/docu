#
# update_host_description CloudForms Automate Method
#
# This method has to run in the context of a vm object. It takes the
# vm name and description to update a VM record in RHV-M.
# It takes credentials and URL from the provider configuration.
# RHV-M has two fields, comment and description, and this method
# fills both of them with the VM description from CloudForms.
#

require 'rest_client'
require 'nokogiri'

def log(level, text)
  # If running outside CloudForms, log to the console.
  # Otherwise, use CloudForm's logging facilities.
  if defined? $evm
    $evm.log(level, text)
  else
    puts text
  end
end

def call_rhv(servername, username, password, action, ref=nil, body_type=:xml, body=nil)
  # This method is taken from ramrexx' CloudFormsPOC github.com repo.
  # It has been slightly modified, it is not doing any response parsing.

  # if ref is a url then use that one instead
  unless ref.nil?
    url = ref if ref.include?('http')
  end
  url ||= "https://#{servername}#{ref}"

  puts url

  params = {
    :method=>action,
    :url=>url,
    :user=>username,
    :password=>password,
    :headers=>{ :content_type=>body_type, :accept=>:xml }
  }

  if body_type == :json
    params[:payload] = JSON.generate(body) if body
  else
    params[:payload] = body if body
  end
  log(:info, "Calling -> RHEVM: #{url} action: #{action} payload: #{params[:payload]}")

  response = RestClient::Request.new(params).execute
  log(:info, "Inspecting -> RHEVM response: #{response.inspect}")
  log(:info, "Inspecting -> RHEVM headers: #{response.headers.inspect}")
  unless response.code == 200 || response.code == 201 || response.code == 202
    raise "Failure <- RHEVM Response: #{response.code}"
  end

  log(:info, "Inspecting response: #{response.inspect}")
  return response
end

def find_vm_by_name(servername, username, password, searchname)
  # Find a VM by name, verify if the result matches the name,
  # and return the reference (URL) to the oVirt API when successful

  ref = "/ovirt-engine/api/vms?search=#{searchname}"
  rhv_response = call_rhv(servername, username, password, :get, ref)
  vms_records = Nokogiri::XML(rhv_response)

  if vms_records.xpath('//vm').count == 0
    log(:error, "Search result found no result on RHV provider #{servername} for VM name #{searchname}")
  elsif vms_records.xpath('//vm').count > 1
    log(:error, "Search result returned multiple entries where one expected on RHV provider #{servername} for VM name #{searchname}")
    return nil
  end

  vmref = ''
  vmname = ''

  vms_records.xpath('//vm').each do |vm|
    vmref = vm['href']
    vm.xpath('name').each do |name|
      vmname = name.text
    end
  end

  # Return the URL (reference) to the VM on RHV when the name on RHV matches the name in CF
  if vmname == searchname
    return vmref
  end

  # Otherwise end with this error - if this happens, the search string needs to be reviewed
  log(:error, "Search result didn't match search string on RHV provider #{servername} for VM name #{searchname} result #{vmname}")
  return nil
end

log(:info, "Updating host description on RHV provider started")

vm = $evm.root['vm']

servername = vm.ext_management_system.hostname 
username = vm.ext_management_system.authentication_userid 
password = vm.ext_management_system.authentication_password 
log(:info, "Working on RHV provider #{servername} with username #{username}, password not shown in log")

vmref = find_vm_by_name(servername, username, password, vm.name)
log(:info, "Working on CF VM name #{vm.name}, found RHV provider reference #{vmref}")

#servername = 'rhvmt.test.ux.int.alte-leipziger.de'
#username = 'admin@internal'

body = Nokogiri.XML("<vm><description>#{vm.description}</description><comment>#{vm.description}</comment></vm>").to_s

response = call_rhv(servername, username, password, :put, vmref, :xml, "#{body}")

exit MIQ_OK
