#
# Description: <Method description here>
#
#
# -------------------------------------------------------
# InfoBlox IPAM - Get IP Address
# -------------------------------------------------------
#
$evm.log("info", "IPAM =====> ********* IPAM - GetIP STARTED *********")

require 'rest_client'
require 'json'
require 'nokogiri'
require 'ipaddr'
require 'base64'

@debug = true
class String
  def string_between marker1, marker2
    self[/#{Regexp.escape(marker1)}(.*?)#{Regexp.escape(marker2)}/m, 1]
  end
end

##################################
# Call Infoblox                  #
##################################

def call_infoblox(action, ref='network', body_hash=nil )
  begin
    url = "https://" + @server + "/wapi/"+ @api_version + "/" + "#{ref}"
    params = {
      :method=>action,
      :url=>url,
      :verify_ssl=>false,
      :headers=>{ :content_type=>:json,
      :accept=>:json,
      :authorization=>"Basic #{Base64.strict_encode64("#{@username}:#{@password}")}"}
      }
    params[:payload] = JSON.generate(body_hash) if body_hash

    $evm.log(:info, "IPAM =====> Calling -> Infoblox:<#{url}> action:<#{action}>  payload:<#{params[:payload]}>")

    response = RestClient::Request.new(params).execute

    unless response.code == 200 || response.code == 201
      raise "Failure response:<#{response.code}>"
    else
      $evm.log(:info, "IPAM =====> Success response:<#{response.code}> From:<#{response.request.url}")
    end
    return JSON.parse(response) rescue (return response)
  rescue RestClient::BadRequest => badrequest
    raise "Bad request: #{badrequest} url: #{url} or possibly wrong api_version: #{@api_version} RESPONSE: #{badrequest.response}"
  end
end

##################################
# Find my environment            #
##################################
def find_environment()
  begin
  # The service dialog should contain information about the target VLAN.
  # The value is a list of key=value pairs, separated by a pipe.
  # The "BlueCat" key should contain the value of the network name in IPAM.

    prov = $evm.root["miq_provision"]

    dialog_vlan = prov.get_option(:dialog_vm_vlan)
    $evm.log('info', "IPAM =====> VLAN list from Dialog: #{dialog_vlan}")
    
    # Parse the key1=value1|key2=value2|...|keyx=valuex into an array:
    vlan_list={}
    t = dialog_vlan.split('|')
    t.each { |u|
      k, v = u.split('=')
      vlan_list[k]=v
    }

    $evm.log('info', "IPAM =====> vlan_list: #{vlan_list}")

    vlan_name = vlan_list['BlueCat'].split(" ")
    @vm_vlan_name = vlan_name[1]
    $evm.log('info', "IPAM =====> vm_vlan_name: #{@vm_vlan_name}")
    

    # Determine DNS domain suffix based on provider. By default,
    # machines go into ux.int.alte-leipziger.de. If they go to the
    # TESTNETZ provider, the suffix is test.ux.int.alte-leipziger.de

    providerid = prov.get_option(:src_ems_id)
    provider = $evm.vmdb("ext_management_system").find_by_id(providerid)
    # $evm.log("info", "IPAM =====> Using provider: #{provider.inspect}")

    lifecycle = prov.get_option(:dialog_alh_linux_lifecycle_name)
    $evm.log('info', "IPAM =====> lifecycle: #{lifecycle}")

    case provider.name
    when 'RHVMGMT', 'RHVMP'
      if lifecycle == "dmz"
        @dnsdomain = 'dmzux.al-h-konzern.de'
        @searchstring = 'lx7d'
      else
        @dnsdomain = 'ux.int.alte-leipziger.de'
        @searchstring = 'lx7'
      end
    when 'RHVMT'
      @dnsdomain = 'test.ux.int.alte-leipziger.de'
      @searchstring = 'lx7t'
    else
      $evm.log('error', 'Unknown provider, cannot determine correct target DNS domain, giving up')
      exit MIQ_ABORT
    end
    $evm.log('info', "IPAM =====> domain: #{@dnsdomain}, searchstring: #{@searchstring}")
  end
end

##################################
# Get Network Infos                #
##################################
def get_ip4_networks_by_hint(vlan_name)
  begin
    # Search Subnet Reference of a given VLAN
    search_filter = "vlan?id=#{vlan_name}"
    return_fields = "&_return_fields=assigned_to"
    query_string =  "#{search_filter}" + "#{return_fields}"
    results = call_infoblox(:get, query_string )

    network_ref_full = results[0]["assigned_to"][0]    # ex:  network/ZG5zLm5ldHdvcmskMTAuMS45OC4wLzI0LzA:10.1.98.0/24/intern
    #network_ref = results[0]["assigned_to"][0].string_between("network/" , ":")
    net_info = network_ref_full.split("/")
    network_info = net_info[1].split(":")
    @network_ref = network_info[0]
    @netaddr = network_info[1]
    @prefix = net_info [2]
    @network = @netaddr + '/' + @prefix
    $evm.log('info', "IPAM =====> Net_info: #{net_info}, Network_info: #{network_info}, Prefix: #{@prefix}, Network: #{@network}  ")

    $evm.log(:info, "IPAM =====> Network_ref_full: #{network_ref_full}, Network_ref: #{@network_ref}")

    if results.count == 0
      $evm.log('error', 'IPAM =====> Network search on IPAM returned no results, cannot continue, search string = ' + vlan_name)
      exit MIQ_ABORT
    elsif results.count > 1
      $evm.log('error', 'IPAM =====> Network search on IPAM returned multiple results, should only be one, cannot continue, search string = ' + vlan_name)
      exit MIQ_ABORT
    end

    return network_ref_full
  end
end

##################################
# Generate Hostname              #
##################################
def generate_next_hostname()
  begin
    # Calculcate the "next hostname" - 
    # The prefix defined from the domain in find_environment() is stored in @searchstring
    # It is: 
    #  - lx7 for Production
    #  - lx7t for TESTNETZ.
    #  - lx7d for DMZ
    # 
    
    search_filter = "record:host?name~=#{@searchstring}"
    return_fields = "&_return_fields=name"
    query_string =  "#{search_filter}" + "#{return_fields}"
    namelist = Array.new
    
    call = call_infoblox(:get, query_string).to_json
    items = JSON.parse(call)
    $evm.log("info", "IPAM =====> Hostnames collected: #{items}")
    
    #namelist[0] = @searchstring + '0000'
    if items.nil?
      $evm.log('info', 'IPAM =====> Searching for host names, we got an empty response, using first hostname of all')
    else
      items.each do |item|
        $evm.log("info", "IPAM =====> item: #{item['name']}")
        if item['name'] =~ /^#{@searchstring}[0-9][0-9][0-9][0-9]/
          namelist << item['name']
          #$evm.log("info", "IPAM =====> Namelist: #{namelist}") if @debug
        end
      end
    end
    $evm.log(:info, "IPAM =====> Body : #{namelist}")
    
    # Get VM name from dialog or generate a new one
    generated_name = namelist.sort.uniq.last.succ
    $evm.log(:info, "IPAM =====> Name generation********** Generated_name: #{generated_name}") if @debug
    vmname = generated_name.split('.')            # Get the simple name
    $evm.log(:info, "IPAM =====> Name generation********** VMname: #{vmname}") if @debug
    if @searchstring == "lx7d" or "lx7t"
        # Use char 4 to 7 because DMZ and Testnetz have 4 chars as identifier (lx7d and lx7t)
        value = vmname[0][4..7]
        $evm.log(:info, "IPAM =====> Name generation********** Using delimiter for DMZ or Testnetz") if @debug
    else
        value = vmname[0][3..6]                   # Use the numeric part of the name
        $evm.log(:info, "IPAM =====> Name generation********** Using delimiter for Prod") if @debug
    end
    $evm.log(:info, "IPAM =====> Name generation********** Value: #{value}") if @debug
    value = value.to_i + 1						  # Increment the value
    $evm.log(:info, "IPAM =====> Name generation********** Incremented Value: #{value}") if @debug
    nextname = @searchstring + value.to_s.rjust(4,"0")         # Then create new name
    $evm.log(:info, "IPAM =====> Name generation********** Nextname: #{nextname}") if @debug
    #nextname = vmname[0]
    $evm.log(:info, "IPAM =====> Name generation********** Generated_name: #{generated_name}, Nextname: #{nextname}") if @debug
    return nextname
  end
end

##################################
# Get IP Address                 #
##################################
def getIP(hostname, ipaddress)
  begin
    url = 'https://' + @connection + '/wapi/' + @api_version + '/record:host'
    content = '{"name":"' + hostname + '","ipv4addrs":[{"ipv4addr":"' + ipaddress + '"}],"configure_for_dns":true}'
    $evm.log("info", "IPAM =====> API Call: #{url} #{content}")
    dooie = RestClient::Request.execute(:url => url, :payload => content, :method => :post, :headers => {:content_type => :json, :accept => :json}, :verify_ssl => false)
    $evm.log("info", "IPAM =====> DOOIE: #{dooie}")
    return true
  rescue Exception => e
    $evm.log("info", e.inspect)
    $evm.log("info", dooie)
    return false
  end
end

##################################
# Next Available IP Address      #
##################################
def nextIP(network)
  begin
    $evm.log("info", "IPAM =====> GetIP --> NextIP on - #{network}")
    url = 'https://' + @connection + '/wapi/' + @api_version + '/' + network + "?_function=next_available_ip"
    dooie = RestClient::Request.execute(:url => url, :method => :post, :verify_ssl => false)
    doc = JSON.parse(dooie)
    nextip=doc['ips'][0]
    $evm.log("info", "IPAM =====> GetIP --> NextIP is - #{nextip}")
    return nextip
  rescue Exception => e
    $evm.log("info", e.inspect)
    $evm.log("info", dooie)
    return false
  end
end

##################################
# Get options: netmask, gateway  #
##################################
def get_options(netaddress)    
  begin
     search_filter = "network?ipv4addr=#{netaddress}"
    return_fields = "&_return_fields=options"
    query_string =  "#{search_filter}" + "#{return_fields}"
    
    call = call_infoblox(:get, query_string).to_json
    items = JSON.parse(call)
    values = Array.new
    $evm.log("info", "IPAM =====> Informations collected =>  Item: #{items}, Call: #{call}") if @debug
    
    #options = items["options"]
    $evm.log("info", "IPAM =====> Options collected => Items: #{items}") if @debug
    # Extract part of the array where Options info are
    items.each do |item|
      values = item
      $evm.log(:info, "IPAM =====> Values: #{values}") if @debug
    end   
    options = values['options'][9]
    $evm.log(:info, "IPAM =====> Options: #{options}, Values: #{values}, Values.gateway #{values['options'][9]}") if @debug
    # Fetch the gateway informations
    @gateway  = options['value']
    $evm.log(:info, "IPAM =====> Options: #{options}, Gateway: #{@gateway}") if @debug
  end
end
  
##################################
# Set Options in prov            #
##################################
def set_prov(prov, hostname, ipaddr, netmask)
  begin
    fqdn = hostname + '.' + @dnsdomain
    $evm.log("info", "IPAM =====> GetIP --> FQDN = #{fqdn}")
    $evm.log("info", "IPAM =====> GetIP --> Hostname = #{hostname}")
    $evm.log("info", "IPAM =====> GetIP --> IP Address =  #{ipaddr}")
    $evm.log("info", "IPAM =====> GetIP -->  Netmask = #{netmask}")
    $evm.log("info", "IPAM =====> GetIP -->  Gateway = #{@gateway}")
    
    prov.set_option(:addr_mode, ["static", "static"])
    prov.set_option(:ip_addr, "#{ipaddr}")
    prov.set_option(:subnet_mask, "#{@prefix}")
    prov.set_option(:subnet_mask, "#{netmask}")
    prov.set_option(:subnet_bitmask, "#{@prefix}")
    prov.set_option(:gateway, "#{@gateway}")

    prov.set_option(:vm_target_name, "#{hostname}")
    prov.set_option(:linux_host_name, "#{hostname}")
    prov.set_option(:vm_target_hostname, "#{hostname}")

    prov.set_option(:vm_name, "#{hostname}")
    prov.set_option(:hostname, "#{fqdn}")
    prov.set_option(:dns_domain, "#{@dnsdomain}")
    #prov.set_option(:host_name, "#{hostname}")
    #prov.set_option(:vm_target_hostname, "#{fqdn}")
    #prov.set_option(:vm_hostname, "#{fqdn}")
    
    # Put the FQDN of the machine on the state
    $evm.set_state_var('vm_fqdn',  "#{fqdn}")

    # Store the IP address in the provisioning object for later use
    $evm.set_state_var('ip_addr', "#{ipaddr}")
    
    # Store Hostname as a custom attribute, used by retirement
    vm = prov.vm_template
    vm.custom_set('IPAM_Hostname', fqdn)
    #$evm.log("info", "IPAM =====> Last Verif --> #{prov.inspect}")

    $evm.log("info", "IPAM =====> Nic parameters --> ip_addr: #{ipaddr}, subnet_mask: #{@prefix}, ")
    return ipaddr

  end
end
  

############################################################
#-----------------------MAIN PROGRAM-----------------------#
############################################################

# Retrieve configuration values from model
@username    = $evm.object['username']
@password    = $evm.object.decrypt('password')
@server      = $evm.object['hostname']
@api_version = 'v2.10.1'
@connection = "#{@username}:#{@password}@#{@server}"

prov = $evm.root["miq_provision"]
  
find_environment()
networkid=get_ip4_networks_by_hint(@vm_vlan_name)
$evm.log("info", "IPAM =====> GetIP --> network = #{networkid}")

#Get the VM name from dialog
@request_hostname = prov.get_option(:vm_name)
$evm.log("info", "IPAM =====> VMname from dialog: #{@request_hostname}")
if @request_hostname =~ /cfme/
  $evm.log("info", "IPAM =====> Using prod branch generate_next_hostname() with hostname #{@request_hostname}") if @debug
  vmtargetname = generate_next_hostname()
elsif @request_hostname.start_with?("lxautotest2")
  $evm.log("info", "IPAM =====> Using lxautotest branch with hostname #{@request_hostname}") if @debug
  vmtargetname = "lxautotest2"
  $evm.log("info", "IPAM =====> Set hostname statically to #{@request_hostname}") if @debug
else
  $evm.log("info", "IPAM =====> Using management branch with hostname #{@request_hostname}") if @debug
  vmtargetname = @request_hostname
end
$evm.log("info", "IPAM =====> GetIP --> Hostname: #{vmtargetname}")
#$evm.log("info", "IPAM =====> #{@method} - Inspecting prov:<#{prov.inspect}>"D-LinuxAL-DNSReader)

nextIPADDR = nextIP(networkid)
result = getIP("#{vmtargetname}.#{@dnsdomain}", nextIPADDR)
$evm.log("info", "IPAM =====> DEBUG (#{result})")
mode = prov.set_option(:addr_mode, ["static", "static"])
ip = prov.set_option(:ip_addr, "#{nextIPADDR}")
mask = prov.set_option(:subnet_mask, "#{@prefix}")
bitmask = prov.set_option(:subnet_bitmask, "#{@prefix}")
gw = prov.set_option(:gateway, "#{@gateway}")
$evm.log("info", "IPAM =====> Target name: #{result}, Mode: #{mode}, IP Address: #{ip}, Mask: #{mask}, Bitmask: #{bitmask}, Gateway: #{gw}")
  
if result ==  true
  $evm.log("info", "IPAM =====> GetIP --> #{vmtargetname}.#{@dnsdomain} with IP Address #{nextIPADDR} created successfully")
  options = get_options(@netaddr)
  if $evm.root["vm"].nil?
    set_prov(prov, vmtargetname, nextIPADDR, @prefix)
    $evm.log("info", "IPAM =====> GetIP --> Set Prov:=> Prov: #{prov}, VM target name: #{vmtargetname}, IP Address: #{nextIPADDR}, Netmask: #{@prefix}, Gateway: #{@gateway}")
  end
elsif result == false
  $evm.log("info", "IPAM =====> GetIP --> #{vmtargetname}.#{@dnsdomain} with IP Address #{nextIPADDR} FAILED")
else
  $evm.log("info", "IPAM =====> GetIP --> unknown error")
end
$evm.log("info", "IPAM =====> #{@method} - Inspecting prov:<#{prov.inspect}>") if @debug
$evm.log("info", "IPAM =====> ********* IPAM - GetIP COMPLETED *********")
exit MIQ_OK

