#
# Description: Delete host from infoblox
# By: JP Joerg
# Date: Oct/2019
#
# -------------------------------------------------------
# InfoBlox IPAM - Release IP Address
# -------------------------------------------------------
#
$evm.log("info", "IPAM =====> ********* IPAM - ReleaseIP STARTED *********")

require 'rest_client'
require 'json'
require 'nokogiri'
require 'ipaddr'
require 'base64'

@debug = true
class String
  def string_between marker1, marker2
    self[/#{Regexp.escape(marker1)}(.*?)#{Regexp.escape(marker2)}/m, 1]
  end
end

##################################
# Call Infoblox                  #
##################################

def call_infoblox(action, ref='network', body_hash=nil )
  begin
    url = "https://" + @server + "/wapi/"+ @api_version + "/" + "#{ref}"
    params = {
      :method=>action,
      :url=>url,
      :verify_ssl=>false,
      :headers=>{ :content_type=>:json,
      :accept=>:json,
      :authorization=>"Basic #{Base64.strict_encode64("#{@username}:#{@password}")}"}
      }
    params[:payload] = JSON.generate(body_hash) if body_hash

    $evm.log(:info, "IPAM =====> Calling -> Infoblox:<#{url}> action:<#{action}>  payload:<#{params[:payload]}>")

    response = RestClient::Request.new(params).execute

    unless response.code == 200 || response.code == 201
      raise "Failure response:<#{response.code}>"
    else
      $evm.log(:info, "IPAM =====> Success response:<#{response.code}> From:<#{response.request.url}")
    end
    return JSON.parse(response) rescue (return response)
  rescue RestClient::BadRequest => badrequest
    raise "Bad request: #{badrequest} url: #{url} or possibly wrong api_version: #{@api_version} RESPONSE: #{badrequest.response}"
  end
end
  
#####################################
# Get ip of the host to be deleted  #
#####################################
def get_ip()
  begin
    
    prov = $evm.root['miq_provision'] || $evm.root['vm'].miq_provision
    ipaddr = prov.get_option(:ip_addr)
    if ipaddr.nil?
        # twinter 2019-10-15: VMs provisioned with BlueCat doesnt have the prov.get_option parameter set (change from dhcp reserved to static). We have to fetch the ip the old way.
        ipaddr = prov.destination.ipaddresses[0]
        $evm.log('info', "Retrieved IP address from VM Object(OLD Provisioned VMs): #{ipaddr}")
    end
    
    if ipaddr.nil?
         $evm.log('info', "Retrieving IP address failed: #{ipaddr}")
        exit MIQ_ABORT
    end

    # Following are not needed 
    vm_name = prov.get_option(:vm_name)
    @hostname = prov.get_option(:hostname)
    domain = prov.get_option(:dns_domain)

    $evm.log("info", "IPAM =====> IP Address =  #{ipaddr}") if @debug
    $evm.log("info", "IPAM =====> Hostname = #{@hostname}") if @debug
    $evm.log("info", "IPAM =====> VM Name = #{vm_name}") if @debug
    $evm.log("info", "IPAM =====> Domain = #{domain}") if @debug
    return ipaddr
  end
end
  

##################################
# Get hosts ref                #
##################################
def get_ref(ip)
  begin
    # Search Subnet Reference of a given VLAN
    #search_filter = "record:host?ipv4addr~=#{ip}"
    search_filter = "record:host?ipv4addr=#{ip}"
    return_fields = ""
    query_string =  "#{search_filter}" + "#{return_fields}"
    call = call_infoblox(:get, query_string).to_json
    # exit if IP not in Infoblox
    if call.nil?
      $evm.log('info', "IPAM =====> Host #{@hostname} not registered in INFOBLOX => Results: #{call}")
      exit MIQ_OK
    end
    
    $evm.log('info', "IPAM =====> Host info => Results: #{call}")
    items = JSON.parse(call)
    values = Array.new
    $evm.log("info", "IPAM =====> Informations collected =>  Item: #{items}, Call: #{call}") if @debug


    # Extract part of the array where Options info are
    items.each do |item|
      values = item
      $evm.log(:info, "IPAM =====> Values: #{values}") if @debug
    end   
    ref = values['_ref']
    $evm.log(:info, "IPAM =====> Ref => Ref: #{ref}") if @debug
    return ref
  end
end

##################################
# Get options: netmask, gateway  #
##################################
def delete_host(ref)
  begin
    search_filter = "#{ref}"
    return_fields = ""
    query_string =  "#{search_filter}" + "#{return_fields}"
    
    call = call_infoblox(:delete, query_string)
    $evm.log("info", "IPAM =====> Host deleted =>  Call: #{call}") if @debug
  end
end

############################################################
#-----------------------MAIN PROGRAM-----------------------#
############################################################

# Retrieve configuration values from model
@username    = $evm.object['username']
@password    = $evm.object.decrypt('password')
@server      = $evm.object['hostname']
@api_version = 'v2.10.1'
@connection = "#{@username}:#{@password}@#{@server}"

ipaddr = get_ip()
$evm.log("info", "IPAM =====> IP: #{ipaddr}") if @debug

# Get the ref and host informations from Infoblox
ref = get_ref(ipaddr)
$evm.log("info", "IPAM =====> REF: #{ref}")

# Delete host from infoblox
result_del = delete_host(ref)
$evm.log("info", "IPAM =====> Result: Host: #{@hostname} with IP: #{ipaddr} DELETED")

$evm.log("info", "IPAM =====> ********* IPAM - ReleaseIP COMPLETED *********")
exit MIQ_OK


