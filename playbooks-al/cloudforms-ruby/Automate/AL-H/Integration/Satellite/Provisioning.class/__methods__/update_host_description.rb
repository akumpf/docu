#
# update_host_description CloudForms Automate Method
#
# This method has to run in the context of a vm object. It takes the
# vm name and description to update a host record in Satellite.
# This method has been tested with Satellite 6.2 and API v2.
#

require 'rest-client'
require 'json'
require 'openssl'
require 'base64'

foreman_host = $evm.object['foreman_host']
foreman_user = $evm.object['foreman_user']
foreman_password = $evm.object.decrypt('foreman_password')
organization_name = $evm.object['organization_name']

@uri_base = "https://#{foreman_host}/api/v2"

@headers = {
  :content_type  => 'application/json',
  :accept        => 'application/json;version=2',
  :authorization => "Basic #{Base64.strict_encode64("#{foreman_user}:#{foreman_password}")}"
}

def query_host_id (querycontent)
  # queryuri: path name related to @uri_base, where to search (hostgroups, locations, ...)
  # queryfield: which field (as in database row) should be searched
  # querycontent: what the queryfield has to match (exact match)

  queryuri = 'hosts'
  queryfield = 'name'

  # Put the search URL together
  url = URI.escape("#{@uri_base}/#{queryuri}?search=#{queryfield}~\"#{querycontent}.\"")

  #$evm.log("info", "url => #{url}")
  puts "url => #{url}"

  request = RestClient::Request.new(
    method: :get,
    url: url,
    headers: @headers,
    verify_ssl: OpenSSL::SSL::VERIFY_NONE
  )

  rest_result = request.execute
  json_parse = JSON.parse(rest_result)

  # The subtotal value is the number of matching results.
  # If it is higher than one, the query got no unique result!
  subtotal = json_parse['subtotal'].to_i

  if subtotal == 0
    $evm.log("info", "query failed, no result #{url}")
    return -1
  elsif subtotal == 1
    id = json_parse['results'][0]['id'].to_s
    return id
  elsif subtotal > 1
    $evm.log("info", "query failed, more than one result #{url}")
    return -1
  end

  #$evm.log("info", "query failed, unknown condition #{url}")
  return -1
end

# Get the VM object and the ID for the Satellite host entry for this VM
vm = $evm.root['vm']
host_id = query_host_id(vm.name)

url = URI.escape("#{@uri_base}/hosts/#{host_id}")

hostvalues = {
  :comment => vm.description
}

begin
  request = RestClient::Request.new(
    method: :put,
    url: url,
    headers: @headers,
    payload: { host: hostvalues }.to_json,
    verify_ssl: OpenSSL::SSL::VERIFY_NONE
  )
  rest_result = request.execute
rescue
  $evm.log('error', "Error when updating host in Satellite, URL = #{uri}, HTTP code = #{request.inspect} REST result = #{rest_result}")
end

exit MIQ_OK
