#
# unregister_from_satellite.rb
# 
# This CloudForms Automate Ruby method removes a host entry
# from a Satellite server via the REST-API.
#

$evm.log('info', 'Method started')

require 'rest-client'
require 'json'
require 'openssl'
require 'base64'

foreman_host     = $evm.object['foreman_host']
foreman_user     = $evm.object['foreman_user']
foreman_password = $evm.object.decrypt('foreman_password')
hostgroup_name     = $evm.object['hostgroup_name']
organization_name  = $evm.object['organization_name']
location_name      = $evm.object['location_name']

prov = $evm.root['miq_provision'] || $evm.root['vm'].miq_provision
vm = $evm.root['vm']
$evm.log("info", "LOG99: VM object: #{vm.inspect}")

#vmname = vm.hostnames[0]

vmname = prov.get_option(:hostname)

if vmname == "" or vmname.nil?
  $evm.log("info", "LOG99: Could not find a FQDN #{vmname}, skipping")
  exit MIQ_OK
end

$evm.log('info', "LOG99: Deleting VM #{vm.name} with Hostname #{vmname} from Satellite")

if vm.platform != 'linux' then
  $evm.log('info', 'LOG99: This is not a Linux VM, skipping deletion of Satellite records')
  exit MIQ_OK
end

@uri_base = "https://#{foreman_host}/api/v2/compliance"

@headers = {
  :content_type  => 'application/json',
  :accept        => 'application/json;version=2',
  :authorization => "Basic #{Base64.strict_encode64("#{foreman_user}:#{foreman_password}")}"
}

def remove_openscap_reports (queryuri,queryfield,querycontent)
	# queryuri: path name related to @uri_base, where to search (hostgroups, locations, ...)
	# queryfield: which field (as in database row) should be searched
	# querycontent: what the queryfield has to match (exact match)

	# Put the search URL together
	url = URI.escape("#{@uri_base}/#{queryuri}?search=#{queryfield}=\"#{querycontent}\"")
	
	$evm.log("info", "LOG99: url => #{url}")

	request = RestClient::Request.new(
		method: :get,
		url: url,
		headers: @headers,
		verify_ssl: OpenSSL::SSL::VERIFY_NONE
	)

	rest_result = request.execute
	json_parse = JSON.parse(rest_result)
	
	# The subtotal value is the number of matching results.
	# If it is higher than one, the query got no unique result!
	subtotal = json_parse['subtotal'].to_i
	
	if subtotal == 0
		$evm.log("info", "LOG99: no reports")
		return 0
	end
	$evm.log("info", "LOG99: removing #{subtotal} reports for #{querycontent}")
	for repid in json_parse['results'] do
		uri = "#{@uri_base}/#{queryuri}/#{repid['id']}"
		$evm.log('info', "LOG99: uri => #{uri}")
		begin
			request = RestClient::Request.new(
				method:     :delete,
				url:        uri,
				headers:    @headers,
				verify_ssl: OpenSSL::SSL::VERIFY_NONE
			)
			rest_result = request.execute
	        rescue
       			$evm.log('error', "LOG99: Error when deleting from Satellite, URL = #{uri}, HTTP code = #{request.inspect} REST result = #{rest_result}")
			exit MIQ_ABORT
		end
	end
	return 0
end

# Remove reports
remove_openscap_reports("arf_reports","host",vmname)


@uri_base = "https://#{foreman_host}/api/v2/hosts"

@headers = {
  :content_type  => 'application/json',
  :accept        => 'application/json;version=2',
  :authorization => "Basic #{Base64.strict_encode64("#{foreman_user}:#{foreman_password}")}"
}
uri = "#{@uri_base}/#{vmname}"
$evm.log('info', "LOG99: uri => #{uri}")

begin
  request = RestClient::Request.new(
    method:     :delete,
    url:        uri,
    headers:    @headers,
    verify_ssl: OpenSSL::SSL::VERIFY_NONE
  )
  rest_result = request.execute
rescue

  $evm.log('error', "LOG99: Error when deleting from Satellite, URL = #{uri}, HTTP code = #{request.inspect} REST result = #{rest_result}")
end

# We exit with MIQ_OK even if deleting from Satellite fails, 
# we want the retirement to continue.

exit MIQ_OK
