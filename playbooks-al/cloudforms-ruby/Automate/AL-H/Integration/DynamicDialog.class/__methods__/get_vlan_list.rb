#
# Description: <Method description here>
#

if $evm.root["service_template"].nil?
    $evm.log("warn", "Could not find the service resources, maybe service catalog item, doesn't have a template assigned?")
    exit MIQ_OK
end

providerid = $evm.root["service_template"].service_resources.first.resource.source.ems_id
cluster = $evm.root["service_template"].service_resources.first.resource.source.ems_cluster_name

$evm.log("info", "Provider ID: #{providerid}")

provider = $evm.vmdb("ext_management_system").find_by_id(providerid)

list_of_vlans={}

case provider.name
when "RHVMGMT"
    list_of_vlans["RHV=AL_VLAN247|Satellite=AL UX-Infra-RHEL7-Mgmt|BlueCat=VLAN 0247"] = "AL_VLAN247 (AL UX-Infra-RHEL7-Mgmt)"
when "RHVMP"
    if cluster == "UCS-DMZ"
        list_of_vlans["RHV=AL_VLAN3102|Satellite=AL UX-DMZ-Linux-Management|BlueCat=VLAN 3102"] = "AL_VLAN3102 (DMZ Linux Management)"
        list_of_vlans["RHV=AL_VLAN3103|Satellite=AL UX-DMZ-SOLR-FRG-Slave|BlueCat=VLAN 3103"] = "AL_VLAN3103 (DMZ SOLR FRG Slave)"
        list_of_vlans["RHV=AL_VLAN3104|Satellite=AL UX-DMZ-SOLR-FRG-Master|BlueCat=VLAN 3104"] = "AL_VLAN3104 (DMZ SOLR FRG Master)"
        list_of_vlans["RHV=AL_VLAN3105|Satellite=AL UX-DMZ-SOLR-PROD-Slave|BlueCat=VLAN 3105"] = "AL_VLAN3105 (DMZ SOLR PROD Slave)"
        list_of_vlans["RHV=AL_VLAN3106|Satellite=AL UX-DMZ-SOLR-PROD-Master|BlueCat=VLAN 3106"] = "AL_VLAN3106 (DMZ SOLR PROD Master)"
        list_of_vlans["RHV=AL_VLAN3108|Satellite=AL UX-DMZ-INSIGN-FRG|BlueCat=VLAN 3108"] = "AL_VLAN3108 (DMZ inSign FRG)"
        list_of_vlans["RHV=AL_VLAN3109|Satellite=AL UX-DMZ-INSIGN-PROD|BlueCat=VLAN 3109"] = "AL_VLAN3109 (DMZ inSign PROD)"
        list_of_vlans["RHV=AL_VLAN3110|Satellite=AL UX-DMZ-SAP-ARIBA-TEST|BlueCat=VLAN 3110"] = "AL_VLAN3110 (DMZ SAP Ariba Test)"
        list_of_vlans["RHV=AL_VLAN3111|Satellite=AL UX-DMZ-SAP-ARIBA-PROD|BlueCat=VLAN 3111"] = "AL_VLAN3111 (DMZ SAP Ariba Prod)"
    else
        list_of_vlans["RHV=AL_VLAN211|Satellite=AL UX-Workload-RHEL7|BlueCat=VLAN 0211"] = "AL_VLAN211 (AL UX-Workload-RHEL7)"
        list_of_vlans["RHV=AL_VLAN245|Satellite=none|BlueCat=VLAN 0245"] = "AL_VLAN245 (none)"
    end
when "RHVMT"
    list_of_vlans["RHV=AL_VLAN130|Satellite=AL UX-Infra-Mgmt-Test|BlueCat=VLAN 0130"] = "AL_VLAN130 (AL UX-Infra-Mgmt-Test)"
end

dialog_field = $evm.object
dialog_field["sort_by"]="description"
dialog_field["sort_order"]="ascending"
dialog_field["data_type"]="string"
dialog_field["required"]="true"

dialog_field["values"]=list_of_vlans

$evm.log("info", "Final list: #{list_of_vlans}")
