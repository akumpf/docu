#
# get_lifecyclr.rb
#
# This CloudForms Automate method fills a drop down list for a service
# dialog with lifecycles. The linux lifecycle is a component of the
# hostgroup in Satellite, and the VMs will be tagged with this 
# lifecycle after provisioning.
# 
# The list of available keys depends on the provider (RHVMP, RHVMT,
# RVHMGMT) and have to match available names of tags in the 
# tag category alh_linux_lifecycle. The description will be taken
# from the tag descriptions.
#

if $evm.root['service_template'].nil?
    $evm.log('warn', 'Could not find the service resources, maybe service catalog item has no a template assigned?')
    exit MIQ_OK
end

providerid = $evm.root['service_template'].service_resources.first.resource.source.ems_id
$evm.log('info', "Provider ID: #{providerid}")

provider = $evm.vmdb('ext_management_system').find_by_id(providerid)
$evm.log('info', "Provider name: #{provider}")

classification = $evm.vmdb(:classification).find_by_name('alh_linux_lifecycle')
$evm.log('info', "classification = #{classification.inspect}")

# It would probably be clever to do this via queries instead of 
# using another hash, but I'm running out of time implementing this.
lifecycle_descriptions = {}
$evm.vmdb(:classification).where(:parent_id => classification.id).each do |tag|
    lifecycle_descriptions[tag.name] = tag.description
end

$evm.log('info', "lifecycle_descriptions = #{lifecycle_descriptions}")

list_of_lifecycles = {}

case provider.name
when 'RHVMGMT'
    list_of_lifecycles['sys'] = lifecycle_descriptions['sys'].to_s
when 'RHVMP'
    list_of_lifecycles['dev']  = lifecycle_descriptions['dev'].to_s
    list_of_lifecycles['qa']   = lifecycle_descriptions['qa'].to_s
    list_of_lifecycles['prod'] = lifecycle_descriptions['prod'].to_s
when 'RHVMT'
    list_of_lifecycles['testnetz'] = 'Testnetz'
end

$evm.log("info", "Final list: #{list_of_lifecycles}")

dialog_field = $evm.object
dialog_field['sort_by']    = 'description'
dialog_field['sort_order'] = 'ascending'
dialog_field['data_type']  = 'string'
dialog_field['required']   = 'true'
dialog_field["values"]     = list_of_lifecycles

exit MIQ_OK
