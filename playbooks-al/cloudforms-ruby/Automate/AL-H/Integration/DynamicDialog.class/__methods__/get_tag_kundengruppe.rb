#
# Description: <Method description here>
#

classification = $evm.vmdb(:classification).find_by_name('alh_kundengruppe')

list_of_tags = {}
$evm.vmdb(:classification).where(:parent_id => classification.id).each do |tag|
  list_of_tags[tag.name] = tag.description
end

dialog_field = $evm.object
dialog_field["sort_by"]="description"
dialog_field["sort_order"]="ascending"
dialog_field["data_type"]="string"
dialog_field["required"]="true"

dialog_field["values"]=list_of_tags

$evm.log("info", "Final list_of_tags: #{list_of_tags}")
