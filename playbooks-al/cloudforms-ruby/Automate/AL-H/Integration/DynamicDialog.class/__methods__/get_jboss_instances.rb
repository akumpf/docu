#
# Description: <Method description here>
#

list_of_instances={}
vm = $evm.root['vm']
$evm.log("info", "${vm.custom_keys()}")

custom_keys = vm.custom_keys()
custom_keys.each { |u| 
   if u.starts_with?('JBoss_') and u.ends_with?('_instance')
      v=vm.custom_get(u)
      list_of_instances[v]=v
   end
} 

dialog_field = $evm.object
dialog_field["sort_by"]="description"
dialog_field["sort_order"]="ascending"
dialog_field["data_type"]="string"
dialog_field["required"]="true"

dialog_field["values"]=list_of_instances

$evm.log("info", "Final list: #{list_of_instances}")
