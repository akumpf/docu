#
# Description: <Method description here>
#

if $evm.root["service_template"].nil?
    $evm.log("warn", "Could not find the service resources, maybe service catalog item, doesn't have a template assigned?")
    exit MIQ_OK
end

providerid = $evm.root["service_template"].service_resources.first.resource.source.ems_id

$evm.log("info", "Provider ID: #{providerid}")

provider = $evm.vmdb("ext_management_system").find_by_id(providerid)

list_of_supportlevels={}

case provider.name
when "RHVMGMT"
    list_of_supportlevels["premium"] = "Premium"
when "RHVMP"
    list_of_supportlevels["premium"] = "Premium"
when "RHVMT"
    list_of_supportlevels["premium"] = "Premium"    
end

dialog_field = $evm.object
dialog_field["sort_by"]="description"
dialog_field["sort_order"]="ascending"
dialog_field["data_type"]="string"
dialog_field["required"]="true"

dialog_field["values"]=list_of_supportlevels

$evm.log("info", "Final list: #{list_of_supportlevels}")
