Playbook to install Cognos Analytics 11 HKV

Survey Variablen

cognos_version - Cognos Version
operation - Operation Modus
cognos_env - Cognos Environment
oracle_instantclient - Oracle Instantclient Version
storage_domain - Storage_Domain
disk_size_in_gb - Cognos Analytics Disk Size
lvcognos_size - LVCOGNOS SIZE
lvcognos_temp_size - LVCOGNOS_TEMP SIZE
idm_hostgruppe - IPA Group - Survey Variable

#################################################

# 11.04.2019
cubes Share hinzugefuegt

# 14.11.2018
Firewall Konfiguration configure_firewall.yml entfernt

