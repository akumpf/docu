resets the password of the root user.
standalone playbook because of future use of different departments (i.e. oracle-admins).  

Steps to follow:
	- Copy the "old" root password to a backup in KeePass
	- Create a new password in KeePass
	- adjust the inventory appropriately, DMZ, Prod, QA, DEV and edit the execptions like RHV hypervisors
	- Start Tower template of this playbook and copy/paste the new password in the survey field of the template
	- password will be updated on the systems
	- check for errors in the Tower job log and correct them (e.g. no shell for sudo available)

TEMPLATE: root-password-reset  
