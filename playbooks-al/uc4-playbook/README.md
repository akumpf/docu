design goals for this playbook:

- remove usage of RPM package
- put all needed files (the tgz from Automic) with a version in the filename to pubdir of lxsat
- use wget to pull the files down
- add a survey to choose the version
- think about a task for updating

