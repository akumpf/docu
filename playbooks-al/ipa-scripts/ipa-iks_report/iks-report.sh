#!/bin/bash

> users-with-no-pw-policy.csv
> admin-users.csv


admins=$(ldapsearch -Y GSSAPI -b "cn=linux_admins,cn=groups,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "member:" | sed 's/member:\suid=\([-_a-z]*\)\,.*/\1/g')
user=$(ldapsearch -h localhost -p 389 -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de"  | grep -e "uid:" | awk '{ print $2 }')

echo "userid, name, group" >> admin-users.csv

for i in $admins
do
	echo "$i, $(ldapsearch -Y GSSAPI -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "cn:" | awk -F ':' '{ print $2 }'), linux_admins" >> admin-users.csv
done


echo "userid, name, passwordexpiration" >> users-with-no-pw-policy.csv

for i in $user
do
	pwdexpiration=$(ldapsearch -Y GSSAPI -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "krbPasswordExpiration:" | sed 's/krbPasswordExpiration\:\s\([0-9]*\).......$/\1/g')

	date=$( (expr $(date --date="$pwdexpiration" +%s) - $(date +%s)) )

	if [ $date -gt 7776000 ]
	then
		echo "$i, $(ldapsearch -Y GSSAPI -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "cn:" | awk -F ':' '{ print $2 }'), $(date --date="$pwdexpiration" +%Y-%m-%d)" >> users-with-no-pw-policy.csv
	fi
done
