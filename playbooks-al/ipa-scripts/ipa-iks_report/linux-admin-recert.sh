#!/bin/bash

date=$(date +%Y-%m)

>/tmp/Linux-Admin-ReCert-$date.csv

admins=$(ldapsearch -Y GSSAPI -b "cn=linux_admins,cn=groups,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "member:" | sed 's/member:\suid=\([-_a-z0-9]*\)\,.*/\1/g')

echo "User, Name, Description" >> /tmp/Linux-Admin-ReCert-$date.csv 

for i in $admins
do
	echo "$i,$(ldapsearch -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "givenName:" | awk -F ':' '{ print $2 }')$(ldapsearch -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "sn:" | awk -F ':' '{ print $2 }'), $(ldapsearch -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "title:" | awk -F ': ' '{ print $2 }')" >> /tmp/Linux-Admin-ReCert-$date.csv
done

mailx -s "Linux Admins Rezertifizierung $date" -a /tmp/Linux-Admin-ReCert-$date.csv -r unix-team@alte-leipziger.de tristan.winter@alte-leipziger.de oliver.nahlen@alte-leipziger.de unix-team@alte-leipziger.de < /tmp/Linux-Admin-ReCert-$date.csv

rm -f /tmp/Linux-Admin-ReCert-$date.csv
