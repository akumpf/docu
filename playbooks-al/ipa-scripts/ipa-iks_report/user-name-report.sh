#!/bin/bash

user=$(ldapsearch -h localhost -p 389 -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de"  | grep -e "uid:" | awk '{ print $2 }')

for i in $user
do

	echo "$i,$(ldapsearch -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "givenName:" | awk -F ':' '{ print $2 }')$(ldapsearch -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "sn:" | awk -F ':' '{ print $2 }')"

done
