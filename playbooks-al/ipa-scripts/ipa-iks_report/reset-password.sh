#!/bin/bash

#user="cwobbe hwindorf mask mbaur mseipp onahlen rparker stroll"
#user="wheinz rschneller mschaaf aboxleitner segerland fhaut tedelmayer"
#user="gprau gklaerle spolonyi dlukinic tfeige jsolarski hjkoch mchristmann al12756 al16251 al12894"
#user="al12445 al13452 al14067 al13683 al13560 al13633 al16150 fcai al16152 al13571"
#user="mulrich khfischer mbohris ahoffmann cgraichen rhoelzinger rbartsch crussell"
#user="dnguyen wrossow wmaas hjwloka rrichter al13555 dbehnke al13040 jfoerster"
user="mschickling cwurster npipp dtavares juboss mmayplate mdecker"

for i in $user
do
	echo $i
	echo "dn: uid=$i,cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" > ldap
	echo "changetype: modify" >>ldap
	echo "replace: krbPasswordExpiration" >>ldap
	echo "krbPasswordExpiration: 20190601100000Z" >>ldap
	ldapmodify -D "cn=Directory Manager" -W < ldap 
done
