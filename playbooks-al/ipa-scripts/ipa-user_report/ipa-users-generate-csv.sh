#!/bin/bash
# - 20181206 twinter
#Generates a report over all IPA users into a .csv file. This script needs to be executed on a machine with ldapsearch installed and a connection to our IPA servers

#Empty the file and define CSV format
echo "Number,Username,Name,Description,Groupmemberships(separated by: #)" > ipa.csv

#Get a list of all IPA users. Uses simple authentication because no special privileges are required to fetch a list of all users.
USERS=$(ldapsearch -h localhost -p 389 -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de"  | grep -e "uid:" | awk '{ print $2 }')

#Counter which increments every loop
COUNTER=1

#For loop over all users
for i in $USERS
do

	#Get the displayname (First name and last name)
	NAME=$(ldapsearch -Y GSSAPI -b "uid=$i,cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" 2>&1 | grep "displayName:" | awk -F ":" '{print $2}')

	#Get the job title / description 
	TITLE=$(ldapsearch -Y GSSAPI -b "uid=$i,cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" 2>&1 | grep "title:" | awk -F ":" '{print $2}')

	#Get a list of Groupmemberships
	MEMBER_OF=$(ldapsearch -Y GSSAPI -b "uid=$i,cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" 2>&1| grep "cn=groups" | grep -v "mepManagedEntry" | sed 's/.*memberOf\:\scn=\([A-Za-z0-9_]*\).*/\1/g')

	#Replace spaces with #
	GROUP_MEMBER=$(echo $MEMBER_OF | sed 's/\s/\#/g')

	#Write the data into the .csv file
	echo "$COUNTER,$i,$NAME,$TITLE,$GROUP_MEMBER" >> ipa.csv

	#Increment counter
	COUNTER=$(expr $COUNTER + 1)
done
