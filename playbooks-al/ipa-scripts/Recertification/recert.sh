#!/bin/bash
# This script has two main options. --refresh-data connects to the 389 server and basically gets everything regarding users, hbac, sudorules and sudo commands. 
# Afterwards this data can be used with the --generate-csv option
#
# We need the generated report for recertification of the user permissions. See Arbeitsanweisung or VAIT for more information about recertification 
#
# 2019-08-20 twinter - Initial creation

# Loop through every command argument
for arg in $@;
do
	if [[ $arg =~ --refresh-data ]]; then

		# Create needed directories if necessary
		if [[ ! -d temp ]]; then
			mkdir temp
			mkdir temp/user
			mkdir temp/sudorules
			mkdir temp/hbacrules
			mkdir temp/hostgroups
			mkdir temp/sudocmds
			mkdir temp/sudocmdgroups
			mkdir temp/userinfo
		fi

		kinit admin@UX.INT.ALTE-LEIPZIGER.DE -k -t /root/ipa-scripts/ipa-admin.keytab

		# Get a list of all users
		#USER=$(ldapsearch -h localhost -p 389 -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de"  | grep -e "uid:" | awk '{ print $2 }' | grep -v "admin")
		USER=$(ipa user-find --disabled=False --pkey-only | grep "User login:" | sed 's/.* //g' | egrep -v "admin|capsule")

		# Save the memberOf information in a user file
		for user in $USER
		do
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$user)" | perl -p00e 's/\r?\n //g' | grep "memberOf:" | awk -F "memberOf: " '{ print $2 }' > temp/user/$user
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$user)" | egrep "displayName:|mail:" | sort | awk -F ": " '{ print $2 }' | tr '\n' ',' | sed 's/,$//g' > temp/userinfo/$user
		done

		# Get everything!
		SUDORULES=$(ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=sudorules,cn=sudo,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "ipaUniqueID:" | awk -F "ipaUniqueID: " '{ print $2 }' | xargs)
		HBACRULES=$(ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=hbac,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "ipaUniqueID:" | awk -F "ipaUniqueID: " '{ print $2 }' | xargs)
		HOSTGROUPS=$(ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=hostgroups,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "dn:" | sed 's/dn: cn=\([a-z0-9_-]*\).*/\1/g')
		# Sudocommands that are referenced by a Sudo command group are stored very strange. We have to fetch only them because the usual sudo commands are identicated by their command. Sudocommands referenced by a command group can only be indentified by ipaUniqueID.
		SUDOCMDSPARTOFGROUP=$(ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=sudocmds,cn=sudo,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "dn: ipaUniqueID" | sed 's/dn: ipaUniqueID=\([a-zA-Z0-9-]*\),cn=.*/\1/g')
		SUDOCMDGROUPS=$(ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=sudocmdgroups,cn=sudo,dc=ux,dc=int,dc=alte-leipziger,dc=de" | grep "cn:" | sed 's/cn: \([a-zA-Z0-9_.-]*\)$/\1/g' | xargs)


		# Loop through gathered data and save everything local so we dont dos our 389 server
		for sudorule in $SUDORULES
		do
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "ipaUniqueID=$sudorule,cn=sudorules,cn=sudo,dc=ux,dc=int,dc=alte-leipziger,dc=de" > temp/sudorules/$sudorule
		done

		for hbacrule in $HBACRULES
		do
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "ipaUniqueID=$hbacrule,cn=hbac,dc=ux,dc=int,dc=alte-leipziger,dc=de" > temp/hbacrules/$hbacrule
		done

		for hostgroup in $HOSTGROUPS
		do
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=$hostgroup,cn=hostgroups,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" > temp/hostgroups/$hostgroup
		done

		for sudocmd in $SUDOCMDSPARTOFGROUP
		do
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "ipaUniqueID=$sudocmd,cn=sudocmds,cn=sudo,dc=ux,dc=int,dc=alte-leipziger,dc=de" > temp/sudocmds/$sudocmd
		done

		for sudocmdgroup in $SUDOCMDGROUPS
		do
			ldapsearch -D "cn=Directory Manager" -w R3dHat10 -b "cn=$sudocmdgroup,cn=sudocmdgroups,cn=sudo,dc=ux,dc=int,dc=alte-leipziger,dc=de" > temp/sudocmdgroups/$sudocmdgroup	
		done

		kdestroy

	# Formatting section. Completely independent of 389
	elif [[ $arg =~ --generate-csv ]]; then

		# Remove any existing data from previous runs
		rm -rf temp/output
		mkdir temp/output

		# Get a list of each user locally
		USER=$(find ./temp/user -type f | awk -F "/" '{ print $4 }' | xargs)	
		#USER="al13742"

		# CSV Header
		echo "Username,Name,Mail,Permissiontype,Systems,User,Commands" > temp/user-recert.csv

		for user in $USER
		do
			# Get all associated HBAC and Sudorules
			USERHBACRULES=$(egrep "hbac" ./temp/user/$user | sed 's/ipaUniqueID=\([a-zA-Z0-9-]*\),cn.*/\1/g' | xargs)
			USERSUDORULES=$(egrep "sudorules" ./temp/user/$user | sed 's/ipaUniqueID=\([a-zA-Z0-9-]*\),cn.*/\1/g' | xargs)
			
			# Empty the ALLOWED_HOSTS array
			ALLOWED_HOSTS=()

			# Loop through HBAC rules to create a list of all servers the user is able to connect to
			for rule in $USERHBACRULES
			do
				Members=$(grep "memberHost:" temp/hbacrules/$rule | sed 's/memberHost: //g' | xargs)
				for member in $Members
					do
						# There are different kind of associations here. Directly attached fqdns and host groups
						if [[ $member =~ fqdn.* ]] ; then
							ALLOWED_HOSTS=("${ALLOWED_HOSTS[@]}" "$(echo $member | sed 's/fqdn=\([a-z0-9.-]*\),cn=.*/\1/g')")
						else
							# If we are a hostgroup we need to further dig for real hostnames
							EXTRACTEDHOSTGROUP=$(echo $member | sed 's/cn=\([a-z0-9_-]*\),cn=.*/\1/g')
							HOSTGROUPMEMBER=$(grep "member:" temp/hostgroups/$EXTRACTEDHOSTGROUP | sed 's/member: fqdn=\([a-z0-9.-]*\),cn.*/\1/g' | xargs)	
							for member in $HOSTGROUPMEMBER
							do
								ALLOWED_HOSTS=("${ALLOWED_HOSTS[@]}" "$member")
							done
						fi
					done
			done	


			# Output the results to an outputfile
			echo "$user,$(cat temp/userinfo/$user),HBAC,${ALLOWED_HOSTS[@]}" > temp/output/$user


			# Loop through Sudo rules to create a list of capabilities. For example: User a can execute /bin/echo as root on lx70256
			for rule in $USERSUDORULES
			do
				# Get the list of hosts attached to sudorule
				Members=$(grep "memberHost:" temp/sudorules/$rule | sed 's/memberHost: //g' | xargs)
				# Get the list of external users attached to sudorule
				EXTUSERS=$(grep "ipaSudoRunAsExtUser:" temp/sudorules/$rule | sed 's/ipaSudoRunAsExtUser: \([a-zA-Z0-9]*\)$/\1/g' | xargs)
				RULEALLOWEDONHOSTS=()
				RULE_ALLOWED_USER=()
				RULE_ALLOWED_CMDS=()

				# If Members is empty search for host categories. Host category will be "all" we need this for linux admins
				if [[ -z $Members ]] ; then
					HOSTCATEGORY=$(grep "hostCategory:" temp/sudorules/e9f7129e-439f-11e3-9e6c-525400782509 | sed 's/hostCategory: //g')
					for cat in $HOSTCATEGORY
					do
						if [[ $cat == all ]]; then
							RULEALLOWEDONHOSTS=("${RULEALLOWEDONHOSTS[@]}" "$cat")
						fi
					done
				else
					for host in $Members
					do
						# Again we have directly attached fqdns and hostgroups
						if [[ $host =~ fqdn.* ]] ; then
							RULEALLOWEDONHOSTS=("${RULEALLOWEDONHOSTS[@]}" "$(echo $host | sed 's/fqdn=\([a-z0-9.-]*\),cn=.*/\1/g')")
						else
							# Extra loop through host group to determine real fqdns
							EXTRACTEDHOSTGROUP=$(echo $host | sed 's/cn=\([a-z0-9_-]*\),cn=.*/\1/g')
							HOSTGROUPMEMBER=$(grep "member:" temp/hostgroups/$EXTRACTEDHOSTGROUP | sed 's/member: fqdn=\([a-z0-9.-]*\),cn.*/\1/g' | xargs)	
							for member in $HOSTGROUPMEMBER
							do
								RULEALLOWEDONHOSTS=("${RULEALLOWEDONHOSTS[@]}" "$member")
							done
						fi
					done
				fi

				# Catch empty hostgroups
				if [ ${#RULEALLOWEDONHOSTS[@]} -eq 0 ] ; then
					RULEALLOWEDONHOSTS=("${RULEALLOWEDONHOSTS[@]}" "No hosts")
				fi


				# Write users to array
				for extuser in $EXTUSERS
				do
					RULE_ALLOWED_USER=("${ALLOWED_USER[@]}" "$extuser")
				done

				# Get the list of allowed commands. We have to replace whitespaces with some distinct random chars
				ALLOWEDCMD=$(grep "memberAllowCmd:" temp/sudorules/$rule |sed 's/memberAllowCmd: //g' | sed 's/ /~+/g') 

				for allowedcmd in $ALLOWEDCMD
				do

					# Reformat ~+ to whitespace again to match the right rules
					allowedcmd=$(echo $allowedcmd | sed 's/~+/ /g')

					# This is a little bit tricky. We have directly attached sudo commands that are simple to fetch. Next option is a command group attached to the rule, they start with cn=
					# Last option is the ipa unique ID. If a sudo command is referenced in a command group, they get stored very strange. The only way to access real command values is via 
					# the unique ID.
					if [[ $allowedcmd =~ sudocmd= ]] ; then
						COMMAND=$(echo $allowedcmd | sed 's/sudocmd=\([a-zA-Z0-9/_ -]*\),cn=.*/\1/g')
						RULE_ALLOWED_CMDS=("${RULE_ALLOWED_CMDS[@]}" "$COMMAND")
					elif [[ $allowedcmd =~ cn=sudocmdgroups ]] ; then
						MEMBEROFCMDGROUP=$(grep "member:" temp/sudocmdgroups/$(echo $allowedcmd | sed 's/cn=\([a-zA-Z0-9_.-]*\),cn=.*/\1/g') | sed 's/member: //g' | sed 's/ /~+/g')
						for member in $MEMBEROFCMDGROUP
						do
							member=$(echo $member | sed 's/~+/ /g')
							if [[ $member =~ sudocmd=.* ]] ; then
								COMMAND=$(echo $member | sed 's/sudocmd=\([a-zA-Z0-9/_ -]*\),cn=.*/\1/g')
								RULE_ALLOWED_CMDS=("${RULE_ALLOWED_CMDS[@]}" "$COMMAND")
							elif [[ $member =~ ipaUniqueID.* ]] ; then
								COMMANDS=$(grep "sudoCmd:" temp/sudocmds/$(echo $member | sed 's/ipaUniqueID=\([a-zA-Z0-9-]*\),cn=.*/\1/g') | sed 's/sudoCmd: //g' | xargs)	
								for cmd in $COMMANDS
								do
									RULE_ALLOWED_CMDS=("${RULE_ALLOWED_CMDS[@]}" "$cmd")
								done
							else
								echo "$rule -- $user"
								echo "Fehler IF cn=.*   --  $member"
							fi
						done
					elif [[ $allowedcmd =~ ipaUniqueID ]] ; then
						COMMANDS=$(grep "sudoCmd:" temp/sudocmds/$(echo $allowedcmd | sed 's/ipaUniqueID=\([a-zA-Z0-9-]*\),cn=.*/\1/g') | sed 's/sudoCmd: //g' | xargs)
						for cmd in $COMMANDS
						do
							RULE_ALLOWED_CMDS=("${RULE_ALLOWED_CMDS[@]}" "$cmd")
						done
					else
						echo "$rule -- $user"
						echo "Fehler IF for loop    --  $allowedcmd"
					fi

				done
				
				# Output the results to an outputfile
				# If the RULE_ALLOWED_CMDS array is empty means there are no specific commands defined. This results in all commands
				if [ ${#RULE_ALLOWED_CMDS[@]} -eq 0 ] ; then
					echo "$user,$(cat temp/userinfo/$user),SUDO,${RULEALLOWEDONHOSTS[@]},${RULE_ALLOWED_USER[@]},all" >> temp/output/$user
				else
					echo "$user,$(cat temp/userinfo/$user),SUDO,${RULEALLOWEDONHOSTS[@]},${RULE_ALLOWED_USER[@]},${RULE_ALLOWED_CMDS[@]}" >> temp/output/$user
				fi

			done


		done
		
		for user in $USER
		do
			cat temp/output/$user >> temp/user-recert.csv
		done

	else
	
		echo "Usage: recert.sh OPTION

  --refresh-data 	Connects to 389 to fetch newest user information
  --generate-csv	Generates csv file based on user information generated with --refresh-data. 
"

	fi
done