#!/bin/sh
#  Ensure users are not created

function ensure_doesnt_exist {
	echo "[INFO] Ensuring user $1 doesn't exist in lxipa1"
	ipa --delegate -c $PWD/lxipa1.conf user-find $1
	if [ $? -eq 0 ]; then
		echo -e "[ERROR] User $1 exists in lxipa1.. exiting\n"
		exit 1
	fi

	echo "[INFO] Ensuring user $1 doesn't exist in lxipa2"
	ipa --delegate -c $PWD/lxipa2.conf user-find $1
	if [ $? -eq 0 ]; then
		echo -e "User $1 exists in lxipa2.. exiting\n"
		exit 1
	fi

	echo "[INFO] Ensuring user $1 doesn't exist in lxipa4"
	ipa --delegate -c $PWD/lxipa4.conf user-find $1
	if [ $? -eq 0 ]; then
		echo -e "User $1 exists in lxipa4.. exiting\n"
		exit 1
	fi
}

function ensure_exists {
	echo "[INFO] Ensuring user $1 exists in lxipa1"
	ipa --delegate -c $PWD/lxipa1.conf user-find $1
	if [ $? -ne 0 ]; then
		echo -e "[ERROR] User $1 doesn't exist in lxipa1.. exiting\n"
		exit 1
	fi

	echo "[INFO] Ensuring user $1 exists in lxipa2"
	ipa --delegate -c $PWD/lxipa2.conf user-find $1
	if [ $? -ne 0 ]; then
		echo -e "[ERROR] User $1 doesn't exist in lxipa2.. exiting\n"
		exit 1
	fi
	
	echo "[INFO] Ensuring user $1 exists in lxipa4"
	ipa --delegate -c $PWD/lxipa4.conf user-find $1
	if [ $? -ne 0 ]; then
		echo -e "[ERROR] User $1 doesn't exist in lxipa4.. exiting\n"
		exit 1
	fi

}


ensure_doesnt_exist testipa1
ensure_doesnt_exist testipa2
ensure_doesnt_exist testipa4

echo -e "[ADD] Create user testipa1 in lxipa1\n"
ipa --delegate -c $PWD/lxipa1.conf user-add --first=Test --last=lxipa1 testipa1 
ensure_exists testipa1

echo -e "[ADD] Create user testipa2 in lxipa2\n"
ipa --delegate -c $PWD/lxipa2.conf user-add --first=Test --last=lxipa2 testipa2 
ensure_exists testipa2

echo -e "[ADD] Create user testipa4 in lxipa4\n"
ipa --delegate -c $PWD/lxipa4.conf user-add --first=Test --last=lxipa4 testipa4 
ensure_exists testipa4

echo -e "[CLEANUP] Delete user testipa1 in lxipa1\n"
ipa --delegate -c $PWD/lxipa1.conf user-del  testipa1
sleep 5
ensure_doesnt_exist testipa1

echo -e "[CLEANUP] Delete user testipa2 in lxipa2\n"
ipa --delegate -c $PWD/lxipa2.conf user-del  testipa2
sleep 5
ensure_doesnt_exist testipa2

echo -e "[CLEANUP] Delete user testipa4 in lxipa4\n"
ipa --delegate -c $PWD/lxipa4.conf user-del  testipa4
sleep 5
ensure_doesnt_exist testipa4

