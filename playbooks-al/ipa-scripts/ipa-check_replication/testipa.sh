#!/bin/sh
#  Ensure users are not created

function ensure_doesnt_exist {
	echo "[INFO] Ensuring user $1 doesn't exist in lxipa201"
	ipa --delegate -c /etc/ipa/lxipa201.conf user-find $1
	if [ $? -eq 0 ]; then
		echo "[ERROR] User $1 exists in lxipa201.. exiting"
		exit 1
	fi

	echo "[INFO] Ensuring user $1 doesn't exist in lxipa202"
	ipa --delegate -c /etc/ipa/lxipa202.conf user-find $1
	if [ $? -eq 0 ]; then
		echo "User $1 exists in lxipa202.. exiting"
		exit 1
	fi
}

function ensure_exists {
	echo "[INFO] Ensuring user $1 exists in lxipa201"
	ipa --delegate -c /etc/ipa/lxipa201.conf user-find $1
	if [ $? -ne 0 ]; then
		echo "[ERROR] User $1 doesn't exist in lxipa201.. exiting"
		exit 1
	fi

	echo "[INFO] Ensuring user $1 exists in lxipa202"
	ipa --delegate -c /etc/ipa/lxipa202.conf user-find $1
	if [ $? -ne 0 ]; then
		echo "User $1 doesn't exist in lxipa202.. exiting"
		exit 1
	fi

}


ensure_doesnt_exist testipa201
ensure_doesnt_exist testipa202

echo "[ADD] Create user testipa201 in lxipa201"
ipa --delegate -c /etc/ipa/lxipa201.conf user-add --first=Test --last=lxipa201 testipa201 
ensure_exists testipa201


echo "[ADD] Create user testipa202 in lxipa202"
ipa --delegate -c /etc/ipa/lxipa202.conf user-add --first=Test --last=lxipa202 testipa202 
ensure_exists testipa202

echo "[CLEANUP] Delete user testipa201 in lxipa201"
ipa --delegate -c /etc/ipa/lxipa201.conf user-del  testipa201
ensure_doesnt_exist testipa201

echo "[CLEANUP] Delete user testipa202 in lxipa202"
ipa --delegate -c /etc/ipa/lxipa202.conf user-del testipa202 
ensure_doesnt_exist testipa202

