function pwdexpiration {
 
    USERNAME=$(id | sed 's/uid=[0-9]*(\([a-z0-9]*\)).*/\1/g')
    RED='\033[0;31m'
    NC='\033[0m'
 
    CURRENTDATETIME=$(date +%Y%m%d)
    PWDEXP=$(ldapsearch -Y GSSAPI -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$USERNAME)" 2>&1 | grep -e "krbPasswordExpiration:" | awk '{ print $2 }'     | sed 's/.......$//g')

    DAYSLEFT=$(( ($(date --date="$PWDEXP" +%s) - $(date --date="$CURRENTDATETIME" +%s) )/(60*60*24) ))
 
    if [ $DAYSLEFT -le 190 ] && [ $DAYSLEFT -gt 0 ]
    then
        echo -e "${RED}Your password expires in $DAYSLEFT days!${NC}"
    fi
 
}
 

cat /etc/motd
grep -q "^$(id -un)" /etc/passwd
LOCALUSER=$?
if [[ $EUID -ne 0 ]]; then echo "                Welcome back, $(getent passwd $USER|cut -d ':' -f 5)"; fi
if [ $LOCALUSER -eq 1 ]; then echo "                $(pwdexpiration)"; fi
echo "                This system is $(uptime -p)"
echo -e "                Last boot was on: $(uptime -s) \n"
