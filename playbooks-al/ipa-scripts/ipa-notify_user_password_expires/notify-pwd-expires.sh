#!/bin/bash
# twinter 2018-09-14 - Initial version
# twinter 2018-09-24 - Added second notification 
# Achtung: Bei der Ausfuehrung werden Mails verschickt. Zum testen den mailx Befehl auskommentieren!

#Get the ipa admin ticket in order to execute below commands
kinit admin@UX.INT.ALTE-LEIPZIGER.DE -k -t /root/ipa-scripts/ipa-admin.keytab

CURRENTDATETIME=$(date +%Y%m%d)

#Get a list of all users
USER=$(ldapsearch -h localhost -p 389 -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de"  | grep -e "uid:" | awk '{ print $2 }' | grep -v "admin" )

for i in $USER
do
	#Get the expiration date and delete the last 7 chars. 
	PWDEXP=$(ldapsearch -Y GSSAPI -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep -e "krbPasswordExpiration:" | awk '{ print $2 }' | sed 's/.......$//g')

	#Calculate the day left until the pw expires. +%s shows the given date in seconds since 1970-01-01 00:00:00 UTC
	DAYSLEFT=$(( ($(date --date="$PWDEXP" +%s) - $(date --date="$CURRENTDATETIME" +%s) )/(60*60*24) ))

	if [ $DAYSLEFT = 14 ] || [ $DAYSLEFT = 2 ]
	then

		#Get user mail address
		MAIL=$(ldapsearch -h localhost -p 389 -x -b "cn=users,cn=accounts,dc=ux,dc=int,dc=alte-leipziger,dc=de" "(uid=$i)" | grep "mail:" | awk '{ print $2 }')

		if [ x$MAIL != x ]
		then
			echo -e "Guten Tag,\n\ndas Passwort fuer Ihren Linux Benutzer $i laeuft in $DAYSLEFT Tagen ab.\n\nBitte aendern Sie Ihr Passwort zeitnah.\n\nIhr Passwort koennen Sie unter: https://lxipa1.ux.int.alte-leipziger.de aendern.\nMehr Informationen und eine Anleitung erhalten Sie im Intranet unter: http://webhg-intra.ux.int.alte-leipziger.de/AL/AL/browse.de/container.0/jpiqaxlqh \n\nMit freundlichen Gruessen\n\nIhr Linux Team" > mail-body

			mailx -r unix-team@alte-leipziger.de -s "Ihr Linux Passwort laeuft in $DAYSLEFT Tagen ab" $MAIL < mail-body
			
			echo "$i - $DAYSLEFT  $MAIL" >> notified-users.log

		else

			echo "$i" >> users-with-no-mail-address
			
		fi
		
	fi	
done

kdestroy

