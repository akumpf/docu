#!/bin/sh
########################################################################
# File:             $$URL: svn://192.168.111.58/gevabs/trunk/product/GEVAZV/3_0/C/Programme/run_scripts/setEnvCppModules.sh $$
# Last check-in:    $$Date: 2013-01-15 13:43:00 +0100 (Tue, 15 Jan 2013) $$
# Last modified by: $$Author: ivogel $$
# Last revision:    $$Rev: 15439 $$
# GEVA ZV Version: GevaBS internal file, v1.0
# ======================================================================
# Copyright ▒ GEVA Business Solutions GmbH
# ======================================================================
# Proprietary license as contractually agreed.
#
# Name:        setEnvCppModules.sh
#
# Purpose:     set CPP environment
#
# AS platform: generic
# OS platform: Linux
#
########################################################################

# GEVAZV_HOME must set in environment variables e.g. .profile
#GEVAZV_HOME=

# only if not set in environment variables
#ORACLE_HOME=
#TNS_ADMIN=

# LD_LIBRARY_PATH with Oracle Fullclient
#LD_LIBRARY_PATH=.:${GEVAZV_HOME}/cppmodules:${ORACLE_HOME}/instantclient:${LD_LIBRARY_PATH}
# LD_LIBRARY_PATH with Oracle Instantclient
LD_LIBRARY_PATH=.:${GEVAZV_HOME}/cppmodules:${ORACLE_HOME}:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# disable output: /dev/null
CPPMODULES_OUTPATH=${GEVAZV_HOME}/trace
export CPPMODULES_OUTPATH

# Path with Oracle Fullclient
#PATH=${GEVAZV_HOME}/cppmodules:${ORACLE_HOME}/bin:${ORACLE_HOME}/lib:$PATH
# Path with Oracle Instantclient
PATH=${GEVAZV_HOME}/cppmodules:${ORACLE_HOME}:${PATH}

export NLS_LANG=GERMAN_GERMANY.WE8ISO8859P15
