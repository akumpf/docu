#!/usr/bin/python
import os,glob,socket
instances = glob.glob("/opt/rh/jboss/*")
print("<style> table { border-collapse: collapse; width: 600px; }\ntable, th, td { border: 1px solid black }</style>")
print("<table>")
print("<th colspan=2><strong>" + socket.gethostname() + "</th>")
for instance in instances:
  instance_name = os.path.basename(instance)
  print("<tr><td colspan=2><strong>Instance: " + instance_name + "</strong></td></tr>")
  jdk = os.path.basename(os.path.realpath("/opt/rh/jboss/" + instance_name + "/jdk"))
  print("<tr><td>JDK version</td><td>" + jdk + "</td></tr>")
  eap = os.path.basename(os.path.realpath("/opt/rh/jboss/" + instance_name + "/eap"))
  print("<tr><td>EAP version</td><td>" + eap  + "</td></tr>")
  instantclient = os.path.basename(os.path.realpath("/opt/rh/jboss/" + instance_name + "/instantclient"))
  if instantclient != "instantclient":
	  print("<td>Instantclient version</td><td>" + instantclient  + "</td></tr>")
  apps = glob.glob("/opt/rh/jboss/" + instance_name + "/eap/standalone/deployments/*.deployed")
  if apps:
    print("<tr><td>Apps:</td><td></td>")
    for app in apps:
      print("<tr><td></td><td>" + os.path.basename(app).replace(".deployed","") + "</td></tr>")
  print("<tr><td>Network</td><td></td>")
  with open("/opt/rh/jboss/" + instance_name + "/eap/standalone/configuration/standalone.conf", "r") as config:
    for line in config:
      if "bind.address=" in line:
        ip = line.split("=")[2].replace('"','')
        print("<tr><td></td><td>IP: " + ip.rstrip() + "</td></tr>")
        try:
	  print("<tr><td>DNS</td><td> " + socket.gethostbyaddr(ip)[0]  + "</td></tr>")
	except: 
	  print("<tr><td>DNS</td><td>No resolving</td></tr>")

print("</table><br/>")
