Playbook for preparing a system to deliver one ore more EAPs to HKV.  

This playbook does (incomplete, state of 13.03.2018):  

- Jenkins Slave -> create user, bashrc, copy jenkins-slave.jar to lxsat/pub, startstopscript, perms on jenkins-home, details in old example -> finished  
- User-Rights: HKV-Admins should stop/start jboss-instance, local sudo-rules for jboss-user to start/stop instance -> finished  
- NOT DONE add VM to IPA-Hostgroup -> not important, last step  
- mount CIFS Share from HKV (X:) -> two mounts, r/w for jboss-user, user stagename in path to logserver, finished  

24.08.2018:
- Remove jenkins Installation.
