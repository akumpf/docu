Kleine Playbooks mit denen von einer Workstation aus die Systeme aktualisiert werden.

* yum-check-update.yml - entspricht yum check-update
* yum-update.yml - entspricht yum -y update
* yum-reboot-nach-update.yml - fuehrt einen Reboot durch

Nutzung:

Zuerst die manuell erstellten Inventories kopieren. Liegen im Git unter git@git.ux.int.alte-leipziger.de:unix-team/patch_inventories.git

Danach die hosts Variable in den Playbooks entsprechend anpassen. Siehe Inventories

Pruefen welche Systeme das Playbook yum-check-update.yml abfragt:

ansible-playbook yum-check-update.yml -i inventory/DEV_RHEL7 --list-hosts

* Aufruf RHEL6: ansible-playbook yum-check-update.yml -i inventory/DEV_RHEL6
* Aufruf RHEL7: ansible-playbook yum-check-update.yml -i inventory/DEV_RHEL7
