This Playbook can do various openssl and related tasks.

Role to create certificate request (csr file) with openssl on Host.
- tag: csr

Role to copy new certificate file from lxtower1:/tmp/csr to host and refresh certificate on system individually. Keep in mind that the new cer file must exist on lxtower1:/tmp/csr !
- tag: cer

Role to add new CA files (konzern-ca.crt and KONZERN-ROOT.crt) to the hosts and run "update-ca-trust".
- tag: trust

Role to install new katello-ca-consumer RPM from satellite. Run this after replacing certificate on lxsat.
- tag: katello-ca-consumer

Author: oliver.nahlen@alte-leipziger.de
Date: 2017-08-24
