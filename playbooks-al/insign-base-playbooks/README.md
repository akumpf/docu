Base playbooks to install/update/configure inSign servers
***** sollte mal aktualisiert werden! *****

Initial installation
- install VM via "initial-config-playbooks"
- install tomcat via "infra-deploy-upstream-tomcat-base" (Port 80! and OpenJDK)
- install httpd and configure tomcat via these playbooks (role: install) 
- deploy application via these playbooks (role: deploy) 

Roles
- install --> initial configuration of inSign VM
- deploy --> initial installation and update of inSign application

Files from Sat5
- /etc/cron.daily/tmpzipwatch
- /etc/cron.daily/tomcat-gc-log
- /etc/httpd24/conf.d/mod_jk.conf => location.conf & hsts.conf
- => have to change location.conf to template for stage-dependant conf?
- /etc/httpd24/conf.d/userdir.conf
- /etc/httpd24/conf.d/welcome.conf
- /etc/httpd24/conf.d/workers.properties
- /etc/httpd24/conf.modules.d/00-base.conf
- /etc/httpd24/conf.modules.d/00-dav.conf
- /etc/httpd24/conf.modules.d/00-mpm.conf
- /etc/httpd24/conf.modules.d/00-proxy.conf
- /etc/httpd24/conf.modules.d/01-cgi.conf
- /etc/httpd24/conf.modules.d/mod_deflate.conf
- /etc/httpd24/conf/httpd.conf
- /etc/logrotate.d/httpd24
- /etc/logrotate.d/insignlogs
- (ToDo) /etc/sysconfig/iptables => fw port 80. still needs hardening.
- /etc/tomcat7/insign-certificate-for-pdf-signing-al.jks
- /etc/tomcat7/logging.properties
- /etc/tomcat7/server.xml
- /etc/tomcat7/tomcat7.conf => insign-tomcat.service

Templates from Sat5 => change rhn.nodename to ansible_hostname!
- /etc/tomcat7/insign.properties

Other files
- /usr/share/tomcat7/lib/sqljdbc42.jar
- status.jsp

already on lxsat - no longer needed
- /usr/lib/jvm/jre-1.8.0-oracle.x86_64/lib/security/local_policy.jar
- /usr/lib/jvm/jre-1.8.0-oracle.x86_64/lib/security/US_export_policy.jar
