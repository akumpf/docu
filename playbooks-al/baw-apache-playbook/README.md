baw-apache-playbooks

Playbook fuer die Installation der BAW Proxy Apacheserver.
BAW = Business Automation Workflow

Survey Variablen

operation - Operation Modus
baw_env - BAW Umgebung

# Verknuepfung URL zu BAW App Server
DigiEingang_AppSrv - ProxyPass URL fuer BAW App Server 
Postk_Online_AppSrv - ProxyPass URL fuer BAW App Server
Postk_Vorgang_AppSrv - ProxyPass URL fuer BAW App Server
IPSGUI_AppSrv - ProxyPass URL fuer BAW App Server 

# Offen - 28.05.2019

- sudo Regeln erstellen
- initial config playbook anpassen. Firewall, hosts, nsswitch

# Erledigt

- Firewall konfigurieren
- Hosts Eintraege hinzufuegen
- nsswitch konfigurieren
- Logrotate httpd anpassen
- Log Verzeichnis /var/log/httpd_baw/ anlegen
- modproxy konfigurieren und kopieren
- httpd.conf anpassen/kopieren
- autoindex.conf anpassen
- welcome.conf anpassen 
