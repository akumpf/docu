Playbook to install DWH Server on RHEL 7 (AL)

#####################################################

Survey Variablen

operation - Operation Modus
dwh_env - DWH Umgebung
idm_hostgroup - IPA Hostgroup
oracle_java_version - Oracle Java Version
oracle_instantclient - Oracle Instantclient Version
storage_domain - Storage Domain
disk_size_app_gb - DWH Disk Size 
disk_size_gdv_gb - GDV Disk Size

#####################################################

- Jenkins Slave connect to Jenkins HKV
- Oracle Instant Client with SQL Loader (zip)
- Automic Agent running under dwh User
- Oracle Java 1.8.0.latest (zip)
- Apache Groovy (zip)
- Disk 2 /app/dwh 20 GB
- Disk 3 /app/dwh/gdv 50 GB
- NFS Share /app/dwh/share

11.06.2018

groovy Installation deaktiviert. Mail von S.Lenz 06.06.2018 15:12
Aus der Rolle install/task/main.yml install_groovy.yml entfernt.
Aus der Rolle update/task/main.yml update_groovy.yml entfernt.
Die restliche Konfig nicht entfernt.

24.08.2018

Jenkins Installation in ein eigenes PB verschoben.
