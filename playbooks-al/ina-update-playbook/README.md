This playbook is used to update and restart the INa appl. in DMZ.
----------
Limit and serialization, resp. is in yml NOT in Tower!

There are two main.yml's for the two stages acceptance and production.

- main_production.yml
- main_acceptance.yml


Two Tower templates for each stage.

The name of the INa war file is the only option to chose and is supplied via template survey.

The war file must be downloaded from ftp.gevasys.de and stored in lxsat pub directory.
