#!/bin/sh
# vom Ole, 22.05.2018
# modified: 28.05.2018 - more detailed variabels for more easy future use - NAHLEN
# modified: 11.09.2018 - OSID, MID und PTID added - NAHLEN/WINTER 
# modified: 01.10.2018 - No update for Hosts with ccv "special" - NAHLEN/WINTER 

# Update rhel 7 host with new ccv, release version and hostgroup in satellite 6.
# read hosts from file (i.e. 2018_3-DEV in script directory).

scriptdir="$(pwd)"

# Stage die aktualisiert werden soll.
ENV="QA"

# Linux Patch-Cycle (year_[1,2,3,4])
CYCLE="2018_4"

# OS release Version, die gesetzt werden soll.
# Stage SYS hat keine minor release version ! Hier darf man bei release nur "7" eintragen.
RELEASE="7.5"

# CVID ist die ID des ccv (z.B.: ccv-prod-os-rhel-7.4) der dem Host zugewiesen weden soll. 
# Die ID ist in der URL zu sehen, wenn man den ccv im Satallite6 anklickt.
CVID="134"

##################################################################
### !!! Folgende Variablen nur bei Release wechsel anpassen !!!###
##################################################################
# OS ID. Operating System ID. Zu finden in der URL https://lxsat.ux.int.alte-leipziger.de/operatingsystems >> Mouse over "RHEL Server x.x".
OSID="12"

# Medium ID of installation media.
MID="15"

# partition table ID. 120 ist die 'AL-Partition_Virtual default' partition table und muss (vermutlich) nicht immer angepasst werden (Nahlen/Winter fragen).
PTID="120"



## Main ######################################################

# Host-Objekt im Satellite mit hammer-cli anpassen.

for host in $(cat $scriptdir/$CYCLE-$ENV); do

  echo $host
  # Get ccv from Host.
  ccv=$(hammer content-host info --name $host --organization AL-H |grep Content)

  # Update Host only if not "special" in ccv. ccv-special-os-rhel-7server is in PROD stage and Hosts with this ccv should not be overwritten with CVID.
  if [[ $ccv != *"special"* ]]; 
  then
     hammer host info --name $host >> $scriptdir/$CYCLE-$ENV-DONE && hammer host update --operatingsystem-id $OSID --hostgroup ${ENV,,}-workload-premium-rhel-$RELEASE --medium-id $MID --partition-table-id $PTID --release-version $RELEASE --content-view-id $CVID --name $host && hammer host info --name $host >> $scriptdir/$CYCLE-$ENV-DONE
  fi


done
