ORG="AL-H"
OS="rhel-7.3"
ENV=$1
TOQUARTER=$2
CVNAME=$3
hammer content-view publish --organization=${ORG} --name=cv-os-${OS}-${TOQUARTER}

function get_cv {
        CVID=$(hammer --csv content-view list --name $1 --organization ${ORG} | grep -vi '^Content View ID,' | awk -F',' '{print $1}' )
        VID=$(hammer content-view version list --content-view-id ${CVID} | awk -F'|' '{print $1}' | sort -n | tac | head -n 1)
        echo $VID
        return 0
}

hammer content-view update --name $CVNAME --organization ${ORG} --component-ids $(get_cv cv-os-${OS}-${TOQUARTER}),$(get_cv cv-app-al-unix-tools),$(get_cv cv-app-epel-7-${TOQUARTER}),$(get_cv cv-app-jws-3-${OS}),$(get_cv cv-app-oracle-java-${OS})
hammer content-view publish --organization=${ORG} --name=$CVNAME
hammer content-view version promote --organization ${ORG} --to-lifecycle-environment ${ENV^^} --id $(get_cv $CVNAME) --force
