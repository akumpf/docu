# Allow rules to selinux
cd /tmp/
cat /var/log/audit/audit.log | audit2allow -M local
checkmodule -M -m -o local.mod local.te
semodule_package -o local.pp -m local.mod
semodule -i local.pp

