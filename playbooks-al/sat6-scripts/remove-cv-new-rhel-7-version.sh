#!/bin/sh
# Delete a Red Hat Enterprise Linux 7 version 
ORG="AL-H"
# New version
VERSION="7.1"

# Generic function to get latest version of CV
function get_cv {
        CVID=$(hammer --csv content-view list --name $1 --organization ${ORG} | grep -vi '^Content View ID,' | awk -F',' '{print $1}' )
        VID=$(hammer content-view version list --content-view-id ${CVID} | awk -F'|' '{print $1}' | sort -n | tac | head -n 1)
        echo $VID
        return 0
}
for LC in sys qa dev prod; do
        hammer content-view remove-from-environment --name "ccv-${LC}-os-rhel-${VERSION}" --organization="${ORG}" --lifecycle-environment "${LC^^}"
        hammer content-view remove-from-environment --name "ccv-${LC}-os-rhel-${VERSION}" --organization="${ORG}" --lifecycle-environment "Library"
	for versionid in $(hammer --csv content-view version list --content-view "ccv-${LC}-os-rhel-${VERSION}" --organization="${ORG}"|grep -v ID |cut -d"," -f1); do
		hammer content-view version delete --id $versionid --organization=${ORG}
	done
	hammer content-view delete --name "ccv-${LC}-os-rhel-${VERSION}" --organization="${ORG}"
done
	
for QUARTER in q1 q2 q3 q4; do
        hammer content-view remove-from-environment --name "cv-os-rhel-${VERSION}-${QUARTER}" --organization="${ORG}" --lifecycle-environment "Library"
	hammer content-view delete --name "cv-os-rhel-${VERSION}-${QUARTER}" --organization="${ORG}"
done

hammer repository-set disable --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server (Kickstart)" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set disable --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server (RPMs)" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set disable --organization "${ORG}" \
 --product "Red Hat Software Collections for RHEL Server" \
 --name "Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set disable --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server - Optional (RPMs)" \
 --releasever "${VERSION}" --basearch "x86_64"


