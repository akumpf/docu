#!/bin/sh
# Script to update filter dates or/and repositories
ORG="AL-H"
# New version
VERSION="7.2"

# Filters
Q1FILTER="1-01-2017"
Q2FILTER="1-04-2017"
Q3FILTER="1-07-2017"
Q4FILTER="1-10-2017"

# Generic function to get latest version of CV
function get_cv {
        CVID=$(hammer --csv content-view list --name $1 --organization ${ORG} | grep -vi '^Content View ID,' | awk -F',' '{print $1}' )
        VID=$(hammer content-view version list --content-view-id ${CVID} | awk -F'|' '{print $1}' | sort -n | tac | head -n 1)
        echo $VID
        return 0
}

echo "Do you want to update the repositories (R) or the filters (F) or both (B)? Answer F/R/B: "
read UPDATE
if [ "$UPDATE" != "F" ] && [ "$UPDATE" !=  "N" ] && [ "$UPDATE" !=  "B" ] ; then
	echo "Error, you should answer F or R or B"
        exit 1
fi
# Update the content view with the correct repositories and create the filters for each quarter

for QUARTER in q1 q2 q3 q4; do
	# Repositories
        if [ "$UPDATE" == "R" ] || [ "$UPDATE" ==  "B" ] ; then
		hammer content-view update --repositories "Red Hat Enterprise Linux 7 Server RPMs x86_64 ${VERSION}","Red Hat Satellite Tools 6.2 for RHEL 7 Server RPMs x86_64","Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server x86_64 ${VERSION}","Red Hat Enterprise Linux 7 Server - Extras RPMs x86_64","Red Hat Enterprise Linux 7 Server - Optional RPMs x86_64 ${VERSION}","Red Hat Enterprise Linux 7 Server - RH Common RPMs x86_64 7Server","Red Hat Enterprise Linux 7 Server - Supplementary RPMs x86_64 ${VERSION}" --name "cv-os-rhel-${VERSION}-${QUARTER}" --organization "${ORG}"
	fi
	# Filters
        if [ "$UPDATE" == "F" ] || [ "$UPDATE" ==  "B" ] ; then
		hammer content-view filter delete --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --name "Exclude erratas for next quarters" --organization "${ORG}"
		if [ $QUARTER == "q1" ]; then
			hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q1FILTER}" --organization "${ORG}"
			hammer content-view filter rule create  --start-date "${Q1FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security
		elif [ $QUARTER == "q2" ]; then
			hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q2FILTER}" --organization "${ORG}"
			hammer content-view filter rule create  --start-date "${Q2FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security

		elif [ $QUARTER == "q3" ]; then
			hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q3FILTER}" --organization "${ORG}"
			hammer content-view filter rule create  --start-date "${Q3FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security
		elif [ $QUARTER == "q4" ]; then
			hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q4FILTER}" --organization "${ORG}"
			hammer content-view filter rule create  --start-date "${Q4FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security
		fi
	fi
        # Publish the version after set the filters or repositories
	hammer content-view publish --organization=${ORG} --name="cv-os-rhel-${VERSION}-${QUARTER}"
done


