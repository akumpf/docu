#!/usr/bin/python2
import sys 
import os
import requests
from datetime import datetime, timedelta

#Script fetches RHSA information from Red Hat Security Data API. The script returns UPDATE if a critical RHSA has been released whitin the last 5 days.
#For more information about Security Data API lookup access.redhat.com

os.environ["HTTPS_PROXY"] = "uxkatello.ux.int.alte-leipziger.de:3128"

RH_API = 'https://access.redhat.com/labs/securitydataapi'

#We need to queries because the api doesnt take important and above...
SEVERITY_q1 = 'critical'
SEVERITY_q2 = 'important'
AFTER_DATE = datetime.now() - timedelta(days=6)
RHSA_count = 0

def get_data(query):

	full_query = RH_API + query

	#print(full_query)

	r = requests.get(full_query)

	if r.status_code != 200:
		print('ERROR')
		sys.exit(1)
	else:
		return r.json()

endpoint = '/cvrf.json'
params_q1 = 'severity=' + SEVERITY_q1 + '&after=' + str(AFTER_DATE.strftime('%Y-%m-%d'))
params_q2 = 'severity=' + SEVERITY_q2 + '&after=' + str(AFTER_DATE.strftime('%Y-%m-%d'))

data_out_q1 = get_data(endpoint + '?' + params_q1)
data_out_q2 = get_data(endpoint + '?' + params_q2)

for advisory in data_out_q1:
	RHSA_count += 1

	#Useful information for future purposes:
	print(advisory['RHSA'])
	#print(advisory['severity'])
	#print(advisory['released_packages'])

for advisory in data_out_q2:
	RHSA_count += 1

	#Useful information for future purposes:
	print(advisory['RHSA'])
	#print(advisory['severity'])
	#print(advisory['released_packages'])


#We need exit codes for jenkins
if RHSA_count >= 1:
	print('UPDATE')
	sys.exit(0)
else:
	#Exit with error if no updates are available, so jenkins does not continue the build
	print('NO_UPDATE')
	sys.exit(1)
