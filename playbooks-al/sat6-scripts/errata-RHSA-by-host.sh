#!/bin/sh
# vom Ole
# Get all RHSA errata by Host. Splunk will monitor these logs.

rm -f /var/log/errata-RHSA-by-host/*

ORG="AL-H"
HID=$(hammer host list --organization ${ORG} |grep -vE 'OPERATING|virt-who|---' | awk '/[a-z.-]/ {print $3}' )


for h in $HID 
do
  SHORTHOST=$( echo $h | cut -d"." -f1 )
  echo $(date) > /var/log/errata-RHSA-by-host/$SHORTHOST  
  echo rhsa-count $(hammer host errata list --host $h |grep security |wc -l) >> /var/log/errata-RHSA-by-host/$SHORTHOST
  
done
