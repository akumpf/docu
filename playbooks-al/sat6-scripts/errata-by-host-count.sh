#!/bin/sh
# vom Ole
# Get count of all errata by Host. Splunk will monitor these logs.

rm -f /var/log/errata-by-host-count/*

today=$(date +%Y-%m-%d)

ORG="AL-H"
HID=$(hammer host list --organization ${ORG} |grep -vE 'OPERATING|virt-who|---' | awk '/[a-z.-]/ {print $3}' )

for h in $HID 
do
  SHORTHOST=$( echo $h | cut -d"." -f1 )
  HC=$(hammer content-host list --organization AL-H --name $h |grep -vE 'INSTALLABLE|----' )
  echo "$HC $today" > /var/log/errata-by-host-count/$SHORTHOST
done

