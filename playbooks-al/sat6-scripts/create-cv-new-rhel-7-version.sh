#!/bin/sh
# Enable a new Red Hat Enterprise Linux 7 version inside Satellite and create the content views for quarter and create a CCV for each LC
ORG="AL-H"
# New version
VERSION="7.2"

# Filters
Q1FILTER="18-01-2017"
Q2FILTER="19-04-2017"
Q3FILTER="19-07-2017"
Q4FILTER="18-10-2017"

# Lifecycle<>quarter
SYS="q1"
DEV="q2"
QA="q3"
PROD="q4"

# Generic function to get latest version of CV
function get_cv {
        CVID=$(hammer --csv content-view list --name $1 --organization ${ORG} | grep -vi '^Content View ID,' | awk -F',' '{print $1}' )
        VID=$(hammer content-view version list --content-view-id ${CVID} | awk -F'|' '{print $1}' | sort -n | tac | head -n 1)
        echo $VID
        return 0
}

# Enable repositories
echo "Enabling repositories.. Ignore warnings Error: 409 Conflict"
hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server (Kickstart)" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server (RPMs)" \
 --releasever "7Server" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server (RPMs)" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}"  \
 --product "Red Hat Enterprise Linux Server"  \
 --name "Red Hat Satellite Tools 6.2 (for RHEL 7 Server) (RPMs)" \
 --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server - RH Common (RPMs)" \
 --releasever "7Server" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Software Collections for RHEL Server" \
 --name "Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server" \
 --releasever "7Server" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Software Collections for RHEL Server" \
 --name "Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server - Optional (RPMs)" \
 --releasever "7Server" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server - Optional (RPMs)" \
 --releasever "${VERSION}" --basearch "x86_64"

hammer repository-set enable  --organization "${ORG}" \
 --product "Red Hat Enterprise Linux Server" \
 --name "Red Hat Enterprise Linux 7 Server - RH Common (RPMs)" \
 --releasever "7Server" --basearch "x86_64"



# Synchronize them

hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server Kickstart x86_64 ${VERSION}"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server RPMs x86_64 7Server"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server RPMs x86_64 ${VERSION}"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Satellite Tools 6.2 for RHEL 7 Server RPMs x86_64"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server - RH Common RPMs x86_64 7Server"

hammer repository synchronize --product "Red Hat Software Collections for RHEL Server" --organization "${ORG}"  --name "Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server x86_64 7Server"
hammer repository synchronize --product "Red Hat Software Collections for RHEL Server" --organization "${ORG}"  --name "Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server x86_64 7.1"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server - Optional RPMs x86_64 7Server"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server - Optional RPMs x86_64 ${VERSION}"
hammer repository synchronize --product "Red Hat Enterprise Linux Server" --organization "${ORG}"  --name "Red Hat Enterprise Linux 7 Server - RH Common RPMs x86_64 7Server"

# Create content views for each quarter (q1,q2,q3,q4)

hammer content-view create --name "cv-os-rhel-${VERSION}-q1"  --organization "${ORG}" --description "Red Hat Enterprise Linux ${VERSION} for Q1"
hammer content-view create --name "cv-os-rhel-${VERSION}-q2"  --organization "${ORG}" --description "Red Hat Enterprise Linux ${VERSION} for Q2"
hammer content-view create --name "cv-os-rhel-${VERSION}-q3"  --organization "${ORG}" --description "Red Hat Enterprise Linux ${VERSION} for Q3"
hammer content-view create --name "cv-os-rhel-${VERSION}-q4"  --organization "${ORG}" --description "Red Hat Enterprise Linux ${VERSION} for Q4"

# Update the content view with the correct repositories and create the filters for each quarter

for QUARTER in q1 q2 q3 q4; do
	# Repositories
	hammer content-view update --repositories "Red Hat Enterprise Linux 7 Server RPMs x86_64 7Server","Red Hat Enterprise Linux 7 Server RPMs x86_64 ${VERSION}","Red Hat Satellite Tools 6.2 for RHEL 7 Server RPMs x86_64","Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server x86_64 7Server","Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7 Server x86_64 ${VERSION}","Red Hat Enterprise Linux 7 Server - Extras RPMs x86_64","Red Hat Enterprise Linux 7 Server - Optional RPMs x86_64 ${VERSION}","Red Hat Enterprise Linux 7 Server - Optional RPMs x86_64 7Server","Red Hat Enterprise Linux 7 Server - RH Common RPMs x86_64 7Server" --name "cv-os-rhel-${VERSION}-${QUARTER}" --organization "${ORG}"
	# Filters
	hammer content-view filter delete --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --name "Exclude erratas for next quarters " --organization "${ORG}"
	if [ $QUARTER == "q1" ]; then
		hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q1FILTER}" --organization "${ORG}"
		hammer content-view filter rule create  --start-date "${Q1FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security
	elif [ $QUARTER == "q2" ]; then
		hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q2FILTER}" --organization "${ORG}"
		hammer content-view filter rule create  --start-date "${Q2FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security

	elif [ $QUARTER == "q3" ]; then
		hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q3FILTER}" --organization "${ORG}"
		hammer content-view filter rule create  --start-date "${Q3FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security
	elif [ $QUARTER == "q4" ]; then
		hammer content-view filter create --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --type erratum --name "Exclude erratas for next quarters " --description "Include erratas until ${Q4FILTER}" --organization "${ORG}"
		hammer content-view filter rule create  --start-date "${Q4FILTER}T23:00:00.000Z" --content-view "cv-os-rhel-${VERSION}-${QUARTER}" --content-view-filter "Exclude erratas for next quarters" --organization "${ORG}" --types enhancement,bugfix,security
	fi
        # Publish the version after set the filters
	hammer content-view publish --organization=${ORG} --name="cv-os-rhel-${VERSION}-${QUARTER}"
done


# Create a CCV, publish it, and promote to the correct LC


# For SYS
hammer content-view create --composite --name "ccv-sys-os-rhel-${VERSION}" --component-ids $(get_cv cv-app-al-unix-tools),$(get_cv cv-os-rhel-${VERSION}-$SYS) --organization=${ORG}
hammer content-view publish --organization=${ORG} --name "ccv-sys-os-rhel-${VERSION}"
hammer content-view version promote --organization ${ORG} --to-lifecycle-environment SYS --id $(get_cv ccv-sys-os-rhel-${VERSION})

# For DEV
hammer content-view create --composite --name "ccv-dev-os-rhel-${VERSION}" --component-ids $(get_cv cv-app-al-unix-tools),$(get_cv cv-os-rhel-${VERSION}-$DEV) --organization=${ORG} 
hammer content-view publish --organization=${ORG} --name "ccv-dev-os-rhel-${VERSION}"
hammer content-view version promote --organization ${ORG} --to-lifecycle-environment DEV --id $(get_cv ccv-dev-os-rhel-${VERSION}) --force

# For QA
hammer content-view create --composite --name "ccv-qa-os-rhel-${VERSION}" --component-ids $(get_cv cv-app-al-unix-tools),$(get_cv cv-os-rhel-${VERSION}-$QA) --organization=${ORG} 
hammer content-view publish --organization=${ORG} --name "ccv-qa-os-rhel-${VERSION}"
hammer content-view version promote --organization ${ORG} --to-lifecycle-environment QA --id $(get_cv ccv-qa-os-rhel-${VERSION}) --force

# For PROD 
hammer content-view create --composite --name "ccv-prod-os-rhel-${VERSION}" --component-ids $(get_cv cv-app-al-unix-tools),$(get_cv cv-os-rhel-${VERSION}-$PROD) --organization=${ORG}
hammer content-view publish --organization=${ORG} --name "ccv-prod-os-rhel-${VERSION}"
hammer content-view version promote --organization ${ORG} --to-lifecycle-environment PROD --id $(get_cv ccv-prod-os-rhel-${VERSION}) --force
