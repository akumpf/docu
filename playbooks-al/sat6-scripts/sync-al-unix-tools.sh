#!/bin/sh
cd /var/www/html/pub/rhel/7/x86_64/
#rpm --resign *.rpm
chown -R apache:apache .
semanage fcontext -a -t httpd_sys_content_t "/var/www/html/pub/(/.*)?"
restorecon -v -R /var/www/html/pub/
createrepo -v .
chown -R apache:apache .

# Satellite 6 part
ORG="AL-H"

echo "Synchronizing repository al-unix-tools-rhel-7"
hammer repository synchronize --organization "${ORG}" --name "al-unix-tools-rhel-7" --product "AL-Unix-Tools" 
echo "Publishing new version of cv-app-al-unix-tools"
hammer content-view publish --name "cv-app-al-unix-tools" --organization "${ORG}"
function get_cv {
        CVID=$(hammer --csv content-view list --name $1 --organization "${ORG}" | grep -vi '^Content View ID,' | awk -F',' '{print $1}' )
        VID=$(hammer content-view version list --content-view-id ${CVID} | awk -F'|' '{print $1}' | sort -n | tac | head -n 1)
        echo $VID
        return 0
}

CVID=$(get_cv cv-app-al-unix-tools)
#Not needed
#hammer content-view version promote --organization "${ORG}" --to-lifecycle-environment "SYS" --id $CVID
# All CCV should contain the al-unix-tools
hammer  --csv content-view list --organization ${ORG} --composite 1 | grep true | while read ccv
do
	CCVID=$(echo $ccv|cut -d"," -f1)
	CCVNAME=$(echo $ccv|cut -d"," -f2)
        echo "Updating CCV $CCVNAME to include latest version of the cv-app-al-unix-tools"
	INFO=$(hammer content-view info --organization ${ORG} --id $CCVID)
	KEEP=$(echo "$INFO" | awk '/Components:/,/Activation Keys/{print}' | grep -v "Name: cv-app-al-unix-tools" | grep "Name:" -B 1 | grep ID | awk '{print $3}'| paste -sd "," -)
	hammer content-view update --id $CCVID --organization ${ORG} --component-ids $(get_cv cv-app-al-unix-tools),${KEEP// /,}
        echo "Publishing new version of $CCVNAME"
	hammer content-view publish --organization=${ORG} --id $CCVID
        ENV=$(echo "$INFO" |  awk '/Lifecycle Environments:/,/Versions:/{print}'|grep -v "Name: Library"|grep Name|awk '{print $2}')
        echo "Promoting $CCVNAME to $ENV"
	hammer content-view version promote --organization ${ORG} --to-lifecycle-environment ${ENV^^} --id $CCVID
done
