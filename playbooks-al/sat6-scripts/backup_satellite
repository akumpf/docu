#!/bin/sh
# Version 1.3
# This is for backing up the satellite 6 server lxsat
#
# _________________________________________________
# !!!!! Dieses Skript liegt im git-Repository !!!!!
# -------------------------------------------------
#
# Workflow:
# 		- execute katello-backup to shutdown satellite and backup config and databases to /backup/sat6dbdump
#       - use xfsdump to backup /var/lib/pulp on sat6dbdump volume on gluster
#		OLD: - ssh to uxrhsquo1, delete old snapshot and execute gluster volume snapshot of sat6pulp and sat6dbdump
#		OLD: - commands are: 
#		OLD:	gluster snapshot delete sat6pulp-snap
#		OLD:	gluster snapshot create sat6pulp-snap sat6pulp no-timestamp
# - Version 0.4: Don't make snapshot of sat6dbdump
# - Version 0.5: changed grepping for status of katello services to ignore Postgres complaining about IPv6
# - Version 0.6: put restarting katello in function, so the script tries to start everything even if backup runs into some errors
# - Version 1.0: removed gluster snapshots for pulp-volume, changed backup of local XFS pulp filesystem to xfsdump
# - Version 1.1: removed gluster volume for backup completly, changed backup destination to a NFS share from ZFS1-Oberursel
# - Version 1.2: replace 'katello-service' command with 'foreman-maintain service' command (new since 6.5.1)
# - Version 1.3: use foreman-maintain backup method for backups. xfsdump is not recommended anymore (L. de Louw, redhat). (Nahlen, 25.07.2019).


# Global configuration
BACKUPDIR="/backup/sat6dbdump"
NOTIFYMAIL="unix-team@alte-leipziger.de"
CURRENTDAY=$(date +%w)

#twinter 2019-08-13
for arg in "$@";
do
        if [[ $arg =~ --override-day[0-6] ]] ; then
                CURRENTDAY=$(echo $arg | sed 's:--override-day::')
        elif [[ $arg =~ --from-incr[0-6] ]] ; then
                FROM=$(echo $arg | sed 's:--from-incr::')
        elif [[ $arg == --help ]] ; then
                echo "Usage:

Execute $0 without any arguments to perform a regular backup. The options listed below are automatically assigned. 
If any errors appeared or for debugging purposes you can use the below options to modify the default behaviour

Weekdays: 0: Sunday
	  1: Monday
	  2: Tuesday
	  3: Wednesday
	  4: Thursday
	  5: Friday
	  6: Saturday

  --override-dayX - Replace X with the weekday the backup should run with.
  --from-incrX    - Replace X with the weekday the incremental backup should use as base backup.
  --help          - Display this help message
"
		exit
	else
		echo "unknown option: $arg"
		echo "try --help for general usage"
		exit
        fi
done

# Set group owner of the directory to "postgres" and permissions to "0770"
chown :postgres ${BACKUPDIR}
chmod 0770 ${BACKUPDIR}
# change to backup directory
cd ${BACKUPDIR}
# remove old backup files from current backup directory
/bin/rm -f $BACKUPDIR/weekday$CURRENTDAY/*

# Clean logfile
> $BACKUPDIR/weekday$CURRENTDAY/backup.log

# Full backup on sunday (weekday 0). Else do incremental backup. ALWAYS offline !
if [[ $CURRENTDAY == 0 ]]; then
 echo "full backup" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log
 foreman-maintain backup offline --assumeyes --preserve-directory $BACKUPDIR/weekday$CURRENTDAY &>> $BACKUPDIR/weekday$CURRENTDAY/backup.log
else


 # twinter 2019-08-13
 # Check if FROM var is set to override latest link
 if [ -z $FROM ]; then
  # debug: show which directory is latest
  echo "Using source for incr backup:" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log
  namei $BACKUPDIR/latest >> $BACKUPDIR/weekday$CURRENTDAY/backup.log

  echo "incr backup" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log
  foreman-maintain backup offline --assumeyes --preserve-directory --incremental $BACKUPDIR/latest $BACKUPDIR/weekday$CURRENTDAY 2>> $BACKUPDIR/weekday$CURRENTDAY/backup.log

 else

  # Format override day
  FROM=$(echo weekday$FROM)
  echo "Making incr from override day: $FROM" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log

  # Debug: log from dir
  echo "Using source for incr backup:" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log
  echo "$BACKUPDIR/$FROM" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log

  echo "incr backup" >> $BACKUPDIR/weekday$CURRENTDAY/backup.log
  foreman-maintain backup offline --assumeyes --preserve-directory --incremental $BACKUPDIR/$FROM $BACKUPDIR/weekday$CURRENTDAY 2>> $BACKUPDIR/weekday$CURRENTDAY/backup.log

 fi
fi

# everything fine; sending success message
if [ $? == 0 ]; then
 STATUS=$(foreman-maintain service status |tail -n 3)
 if [[ $STATUS =~ "All services are running" ]]; then
  rm -f $BACKUPDIR/latest
  ln -s $BACKUPDIR/weekday$CURRENTDAY $BACKUPDIR/latest
  echo "**** BACKUP Complete, contents can be found in: ${BACKUPDIR} ****" | mail -s "[satellite-backup] SUCCESS backup $(date +%Y-%m-%d-%H:%M:%S)" ${NOTIFYMAIL}
 fi
else
 echo "**** Problem with BACKUP, contents can be found in: ${BACKUPDIR} ****" | mail -s "[satellite-backup] ERROR backup $(date +%Y-%m-%d-%H:%M:%S)" ${NOTIFYMAIL}
fi
