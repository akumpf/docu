ORG="AL-H"
ENV="sys"
OS="rhel-7.2"
QUARTER="q4"
hammer content-view publish --organization=${ORG} --name=cv-os-${OS}-${QUARTER}

function get_cv {
        CVID=$(hammer --csv content-view list --name $1 --organization ${ORG} | grep -vi '^Content View ID,' | awk -F',' '{print $1}' )
        VID=$(hammer content-view version list --content-view-id ${CVID} | awk -F'|' '{print $1}' | sort -n | tac | head -n 1)
        echo $VID
        return 0
}

hammer content-view update --name ccv-${ENV}-os-${OS} --organization ${ORG} --component-ids $(get_cv cv-os-${OS}-${QUARTER}),$(get_cv cv-app-al-unix-tools)
hammer content-view publish --organization=${ORG} --name=ccv-${ENV}-os-${OS}
hammer content-view version promote --organization ${ORG} --to-lifecycle-environment ${ENV^^} --id $(get_cv ccv-${ENV}-os-${OS})

