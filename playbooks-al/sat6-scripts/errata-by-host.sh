#!/bin/sh
# vom Ole
# Get all errata by Host. Splunk will monitor these logs.

rm -f /var/log/errata-by-host/*

ORG="AL-H"
HID=$(hammer host list --organization ${ORG} |grep -vE 'OPERATING|virt-who|---' | awk '/[a-z.-]/ {print $3}' )


for h in $HID
do
  SHORTHOST=$( echo $h | cut -d"." -f1 )
  echo $(date) > /var/log/errata-by-host/$SHORTHOST
  hammer host errata list --host $h |grep -vE 'ERRATUM|----' > /var/log/errata-by-host/$SHORTHOST
done




