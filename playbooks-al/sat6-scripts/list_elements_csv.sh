#!/bin/sh
# Syntax: ./list_elements_csv.sh > report.csv
ORG="AL-H"

# Function to get the repository name

function get_repo_name {
	echo $(hammer repository  info --id $1 --organization "${ORG}"|grep ^Name:|cut -d":" -f2|xargs)
}

echo ",List of activation keys and (C)CV related"
hammer --csv activation-key list --organization "${ORG}"
echo ",List of hostgroups"
hammer --csv hostgroup list --organization "${ORG}"|while read hg
do
	HGID=$(echo $hg | cut -d"," -f1)
	if [ "$HGID" != "Id" ]; then
		HGINFO=$(hammer hostgroup info  --id $HGID|grep -e Subnet: -e Partition -e Medium -e activation|sed -e 's/Subnet:\s*//' -e 's/Partition Table:\s*//' -e 's/Medium:\s*//' -e 's/\s*kt_activation_keys => //'|paste -s -d,)
		echo $(echo $hg|cut -d, -f1-4),$HGINFO
	else
		echo "ID,Name,Title,Operating System,Subnet,Partition,Medium,Activation key"
	fi
done

echo ",List of the Composite Content Views"
hammer --csv content-view list --composite 1 --organization "${ORG}" | while read ccv
do
	CCVID=$(echo $ccv | cut -d"," -f1)
	if [ "$CCVID" != "Content View ID" ]; then
		COMPONENTS=$(hammer content-view  info --id $CCVID --organization "${ORG}"|awk '/Components/,/Activation Keys/'|grep Name|cut -d":" -f2-|sed 's/^ //'|sort|paste -s -d,)
		echo $(echo $ccv|cut -d"," -f1-3),$COMPONENTS
	else
		echo "ID,Name,Label,CV associated1,CV associated2,CV associated3,CV associated4,CV associatedN"
	fi
done


echo ",List of the Composite Content Views"
hammer --csv content-view list --noncomposite 1 --organization "${ORG}" | while read cv
do
	REPOS=$(echo $cv|cut -d"," -f5-)
        NAMES=""
	if [ "$REPOS" != "Repository IDs" ] && [ "$REPOS" != '""' ]; then
		for repo in $REPOS; do
			REPOID=$(echo $repo|tr -d ,|tr -d \")
			NAMES="$(get_repo_name $REPOID),$NAMES"
		done
		echo $(echo $cv|cut -d"," -f1-3),$NAMES
	else
		if [ "$REPOS" != '""' ]; then
			echo "ID,Name,Label,Repository1,Repository2,Repository3,Repository4,Repository5,Repository6,RepositoryN" 
		fi
	fi
done
