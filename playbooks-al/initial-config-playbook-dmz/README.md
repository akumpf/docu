Initial config playbook for DMZ only to use after provisioning a host with satellite.
* Install+config Backup for DMZ (currently TSM)
* Install+config Monitoring for DMZ (currently check-mk)
* Install+config Logger for DMZ (currently Splunk forwarder)
* Configure MOTD for DMZ
