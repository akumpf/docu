#!/bin/bash

kinit -t /etc/tower/enroll.keytab -k enroll@UX.INT.ALTE-LEIPZIGER.DE

if [[ $1 == "pub" ]];
then
	ipa user-find --raw --in-groups=linux_admins --pkey-only|grep "ipasshpubkey" -B 1 --no-group-separator|paste -s -d",\n"
fi

if [[ $1 == "user" ]];
then
	ipa user-find --raw --in-groups=linux_admins --pkey-only|grep "ipasshpubkey" -B 1 --no-group-separator|grep uid|awk '{print $2}'
fi
