gpg --batch --yes --passphrase secret --symmetric hosts

gpg --output hosts.dec --batch --yes --passphrase secret --decrypt hosts.gpg

