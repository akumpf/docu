#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: bluecat 

short_description: bluecat module to allocate/release ip from BlueCat IPAM

version_added: "2.4"

description:
    - "This module uses zeep to make SOAP calls to allocate/release IP from BlueCat system"

options:
    action:
        description:
            - Action can be: login, allocate or release
        required: true
    name:
        description:
            - Allocate or release specified host to/from bluecat
        required: true
    network:
        description:
            - Name of the network where to allocate the IP (mandatory if action is allocate)
        required: false 

    mac:
        description:
            - Allocate specified mac address to bluecat 
        required: false  
    offset:
        description:
            - Use to specify from which address to start to assign
        required: false
    cname:
        description:
            - If DNS name should be an alias, then specify here the destination
        required: false
    username:

    username:
        description:
            - Specify the username to login, required when action=login
        required: false

    password:
        description:
            - Specify the password to login, required when action=login
        required: false

    configuration:
        description:
            - Specify the configuration to use, required when action=login
        required: false



author:
    - Alberto Gonzalez (alberto.gonzalez@redhat.com)
'''

EXAMPLES = '''
# Allocate a new IP
- name: Allocate a new ip
  bluecat:
    action: allocate
    name: testalberto.test.ux.int.alte-leipziger.de
    mac: 00:11:22:33:44:55
    network: VLAN 0130
    offset: 10.1.133.30

# Allocate a new CNAME
- name: Allocate a new cname
  bluecat:
    action: allocate
    name: testalberto.test.ux.int.alte-leipziger.de
    cname: lxautotest2.ux.int.alte-leipziger.de


# Release an IP address
- name: Delete the IP address
  bluecat:
    action: allocate
    name: testalberto.test.ux.int.alte-leipziger.de
'''

RETURN = '''
result:
    description: Result information if needed
'''

from ansible.module_utils.basic import AnsibleModule
from zeep import Client

class BlueCat(object):
  def __init__(self):
    self.logged = False
    self.config = {}
    self.view = {}

  def login(self, username, password, configname):
    self.logged = client.service.login(username, password)
    self.config = client.service.getEntityByName(0, configname, "Configuration")

  def set_view(self, viewname):
    self.view = client.service.getEntityByName(self.config.id, viewname, "View")


  def lookup_server(self, servername):
    record_search = client.service.getHostRecordsByHint(start=0, count=10, options="hint=^%s$|retrieveFields=true" % servername)

  def lookup_hostname(self, hostname):
    record_search = client.service.getHostRecordsByHint(start=0, count=1, options="hint=^%s$|retrieveFields=true" % hostname)
    if not record_search:
      return [None,None]
    record_link = client.service.getLinkedEntities(record_search[0].id, "IP4Address", 0, 1)
    record = client.service.getEntityById(record_link[0].id)
    record_properties = record.properties.split("|")
    record_values = {}
    for prop in record_properties:
      if prop:
              record_values[prop.split("=")[0]] = prop.split("=")[1]
    return [record,record_values]

  def lookup_ip(self, ipaddress):
    record_search = client.service.getIP4Address(self.config.id, ipaddress)
    record_link = client.service.getLinkedEntities(record_search.id, "HostRecord", 0, 1)
    record = client.service.getEntityById(record_link[0].id)
    record_properties = record.properties.split("|")
    record_values = {}
    for prop in record_properties:
      if prop:
              record_values[prop.split("=")[0]] = prop.split("=")[1]
    return [record,record_values]

  def lookup_network(self, network):
    record_search = client.service.getIP4NetworksByHint(self.config.id, 0, 1, options="hint=%s|retrieveFields=true" % network)
    record = None
    if record_search:
            record = record_search[0]
    return record


  def allocate_ip(self, network_id, name, mac, options):
    if mac:
      answer = client.service.assignNextAvailableIP4Address(self.config.id, \
         network_id, mac, "%s,%s,true,false" % (name, self.view.id), "MAKE_DHCP_RESERVED", options) 
    else:
      answer = client.service.assignNextAvailableIP4Address(self.config.id, \
         network_id, "", "%s,%s,true,false" % (name, self.view.id), "MAKE_STATIC", options) 

    return answer.properties

  def allocate_cname(self, network_id, name, cname, options):
    answer = client.service.addAliasRecord(self.view.id, \
         name, cname, 1800, properties="") 

    return answer



  def delete_record(self, record_id):
    answer = client.service.delete(record_id)
    return answer

  def deploy_network(self, network_id):
    #client.service.quickDeploy(network_id,"services=DNS")
    server_id = client.service.getServerForRole(client.service.getDeploymentRoles(network_id)[0].id).id
    client.service.deployServerServices(server_id,"services=DHCP")
    client.service.deployServerServices(server_id,"services=DNS")

  def logout(self):
    client.service.logout()

def run_module():
    # define the available arguments/parameters that a user can pass to
    # the module
    global client
    module_args = dict(
        action=dict(required=True,choices=["login","allocate","release"]),
        name=dict(type='str', required=False),
        network=dict(type='str', required=False),
        mac=dict(type='str', required=False),
        offset=dict(type='str', required=False),
        cname=dict(type='str', required=False),
        username=dict(type='str', required=True),
        server=dict(type='str', required=True),
        password=dict(type='str', required=True),
        configuration=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        result=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result

    for required in ["username", "password", "configuration", "server"]:
       if required not in module.params:
          module.fail_json(msg='You need to specify server, username, password and configuration', **result)
    client = Client('http://%s/Services/API?WSDL' % module.params["server"])
    bc = BlueCat()
    bc.login(module.params["username"],module.params["password"],module.params["configuration"])
    bc.set_view("intern")
    if module.params['action'] == "allocate":
      if "cname" in module.params and module.params["cname"]:
         network = bc.lookup_network("VLAN 0130")
         bc.allocate_cname(network.id, module.params['name'], module.params['cname'], "")
      else:
         if "network" not in module.params:
            module.fail_json(msg='With allocate action you need to specify network', **result)
         host_data, host_props = bc.lookup_hostname(module.params['name'])
         if host_data:
           if "macAddress"in host_props:
              if host_props['macAddress'].replace("-",":") != module.params['mac']:
                 module.fail_json(msg='Hostname has a different mac address associated', **result)
         else: 
           network = bc.lookup_network(module.params['network'])
           if not network:
             module.fail_json(msg='Network not found', **result)
           offset = ""
           if module.params['offset']:
                   offset = "offset=" + module.params['offset'] 
           r = bc.allocate_ip(network.id, module.params['name'], module.params['mac'], offset)
           bc.deploy_network(network.id)
           result['changed'] = True
           result['result'] = r
     
    if module.params['action'] == "release":
      host_data, host_props = bc.lookup_hostname(module.params['name'])
      if host_data:
        ip_data, ip_props = bc.lookup_ip(host_props['address'])
        r = bc.delete_record(host_data.id)
        result['changed'] = True
        result['result'] = r
      network = bc.lookup_network(module.params['network'])
      bc.deploy_network(network.id)
    bc.logout  
 
    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()

