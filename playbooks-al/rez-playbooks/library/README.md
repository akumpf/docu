In your Git project, execute:

git submodule add git@git.ux.int.alte-leipziger.de:unix-team/ansible-bluecat-module.git library

To read the ansible documentation for this module:

ansible-doc  -M library/ bluecat