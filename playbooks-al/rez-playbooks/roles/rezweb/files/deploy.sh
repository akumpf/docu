#!/bin/bash
#
[ $# -ne 1 ] && exec echo "usage: $(basename $0) CONTEXT"
#
CONTEXT=$1
rm -rf /app/rezweb/${CONTEXT}/webapps/${CONTEXT}.war
sleep 5
rm -rf /app/rezweb/${CONTEXT}/work/Catalina/localhost/${CONTEXT}
cp -f /app/rezweb/xfer/${CONTEXT}/rez2g.war /app/rezweb/${CONTEXT}/webapps/${CONTEXT}.war
mv -f /app/rezweb/xfer/${CONTEXT}/rez2g.war /app/rezweb/xfer/${CONTEXT}/BACKUP_$(date '+%d%m%Y_%H%M')-rez2g.war
mv -f /app/rezweb/${CONTEXT}/logs/catalina.out /app/rezweb/xfer/$(date '+%d%m%Y_%H%M')-${CONTEXT}-catalina.out
touch /app/rezweb/${CONTEXT}/logs/catalina.out
#test -f /app/rezweb/${CONTEXT}/logs/rez2g.log && mv -f /app/rezweb/${CONTEXT}/logs/rez2g.log /app/rezweb/xfer/save/$(date '+%d%m%Y_%H%M')-${CONTEXT}-rez2g.log
