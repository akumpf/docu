baw-base-playbooks

Playbook fuer die Installation der BAW Appserver.
BAW = Business Automation Workflow
Alter Namen bpm

Survey Variablen

operation - Operation Modus
baw_env - BAW Umgebung
idm_hostgruppe - IPA Hostgroup
storage_domain - Storage Domain
disk_size_app_gb - BAW Disk Size 

# Offen 
- Der Kram heisst jetzt baw.
  Namen entsprechend anpassen
- X Laufwerk mounten

# Erledigt
- rhel-7-server-eus-optional-rpms Channel fuer weitere Packages aktivieren
- Packete anchinstallieren
  PackageKit-gtk3-module webkitgtk3 libcanberra PackageKit-gtk3-module.i686 webkitgtk3.i686 libcanberra.i686 gtk2 gtk2.i686 gtk3.i686 libXtst.i686 libXtst libXft.i686 adwaita*theme.i686 adwaita*theme xorg-x11-fonts-Type1 compat-libstdc++-33
- baw User anlegen
- Zusaetzliche Disk mit Dateisystem unter /opt/IBM einhaengen
- Zusaetzliche Disk mit Dateisystem unter /var/log/bawlog einhaengen.
  |=> Eine Disk mit zwei LVs genuegt.
- Zusaetzliche Pakete fuer X
- Anpassen der /etc/hosts
- Anpassen der /etc/nsswitch.conf
- Firewall Regeln anlegen
  |=> Ist im initial-config Playbook
- Jenkins installieren (Eigenes PB)
  |=> Eigenes Playbook
- Ulimits /etc/security/limits.d/baw.conf
- Sudo Rules fuer baw
