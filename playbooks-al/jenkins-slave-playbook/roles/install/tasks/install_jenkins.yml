#
# appl_group - Survey Variable
# 
# Die IPA Konfiguration befindet sich in tasks/configure_ipa_hostgroup.yml
#

# Checks

- name: install_jenkins.yml | Check if Directory exists
  stat:
    path: /opt/rh
  register: rh_directory
  ignore_errors: true

- name: install_jenkins.yml | Check if User apache exists
  getent:
    database: passwd
    key: apache
  register: apache_existiert
  ignore_errors: true
      
- name: install_jenkins.yml | Check if User jboss exists
  getent:
    database: passwd
    key: jboss
  register: jboss_existiert
  ignore_errors: true

# Set Groupmembership

- name: install_jenkins.yml | Set Jenkins Groupmembership apache,jboss,"{{ appl_group }}"
  set_fact:
    jenkins_gruppen: apache,jboss,"{{ appl_group }}"
  when:
    - apache_existiert.failed == false
    - jboss_existiert.failed == false
    - appl_group != ""

- name: install_jenkins.yml | Set Jenkins Groupmembership jboss,"{{ appl_group }}"
  set_fact:
    jenkins_gruppen: jboss,"{{ appl_group }}"
  when:
    - apache_existiert.failed == true
    - jboss_existiert.failed == false
    - appl_group != ""

- name: install_jenkins.yml | Set Jenkins Groupmembership apache,"{{ appl_group }}"
  set_fact:
    jenkins_gruppen: apache,"{{ appl_group }}"
  when:
    - apache_existiert.failed == false
    - jboss_existiert.failed == true
    - appl_group != ""

- name: install_jenkins.yml | Set Jenkins Groupmembership "{{ appl_group }}"
  set_fact:
    jenkins_gruppen: "{{ appl_group }}"
  when:
    - apache_existiert.failed == true
    - jboss_existiert.failed == true
    - appl_group != ""

- name: install_jenkins.yml | Set Jenkins Groupmembership apache,jboss
  set_fact:
    jenkins_gruppen: apache,jboss
  when:
    - apache_existiert.failed == false
    - jboss_existiert.failed == false
    - appl_group == ""

- name: install_jenkins.yml | Set Jenkins Groupmembership jboss
  set_fact:
    jenkins_gruppen: jboss
  when:
    - apache_existiert.failed == true
    - jboss_existiert.failed == false
    - appl_group == ""

- name: install_jenkins.yml | Set Jenkins Groupmembership apache
  set_fact:
    jenkins_gruppen: apache
  when:
    - apache_existiert.failed == false
    - jboss_existiert.failed == true
    - appl_group == ""

# Tasks

- name: install_jenkins.yml | Make sure that /tmp is big enough
  command: lvextend -r -L 1.5g {{ item.device }}
  with_items: "{{ ansible_mounts }}"
  when: item.mount == "/tmp"

- name: install_jenkins.yml | Create Directory for jenkins home
  file:
    path: "/opt/rh"
    state: "directory"
    owner: root
    group: root
    mode: "755"
  when: rh_directory.stat.exists == false

- name: install_jenkins.yml | Create jenkins User
  user:
    name: "jenkins"
    system: "yes"
    state: "present"
    shell: "/sbin/nologin"
    groups: "{{ jenkins_gruppen }}"
    home: /opt/rh/jenkins
    comment: "User for jenkins agent"

- name: install_jenkins.yml | Copy .bashrc to jenkins User
  template:
    src: "jenkins.bashrc"
    dest: "/opt/rh/jenkins/.bashrc"
    owner: "jenkins"
    group: "jenkins"
    mode: 0644

- name: install_jenkins.yml | Copy .bash_profile to jenkins User
  template:
    src: "jenkins.bash_profile"
    dest: "/opt/rh/jenkins/.bash_profile"
    owner: "jenkins"
    group: "jenkins"
    mode: 0644

- name: install_jenkins.yml | Copy Jenkins Agent to jenkins User
  get_url:
    url: "{{ url_software_jenkins }}{{ jenkins_jar_file }}"
    dest: "/opt/rh/jenkins/slave.jar"
    owner: "jenkins"
    group: "jenkins"
    mode: 0644

- name: install_jenkins.yml | Copy Jenkins Agent start script
  template:
    src: "startjenkinsslave.sh.j2"
    dest: "/opt/rh/jenkins/startjenkinsslave.sh"
    owner: "jenkins"
    group: "jenkins"
    force: no
    mode: 0744

- name: install_jenkins.yml | Copy Jenkins Agent stop script
  copy:
    src: "stopjenkinsslave.sh"
    dest: "/opt/rh/jenkins/stopjenkinsslave.sh"
    owner: "jenkins"
    group: "jenkins"
    mode: 0744

- name: install_jenkins.yml | Open jenkins directory to all other users
  file:
    path: "/opt/rh/jenkins"
    state: "directory"
    mode: "0755"
    owner: "jenkins"
    group: "jenkins"

- name: install_jenkins.yml | Allow jenkins user to start and stop agent
  lineinfile:
    dest: /etc/sudoers.d/jenkins_user
    state: present
    create: True
    line: "{{ item }}"
    validate: 'visudo -cf %s'
  with_items:
    - 'Defaults: jenkins !requiretty'
    - 'jenkins ALL=(ALL)  NOPASSWD: /bin/systemctl start jenkins-agent'
    - 'jenkins ALL=(ALL)  NOPASSWD: /bin/systemctl stop jenkins-agent'
    - 'jenkins ALL=(ALL)  NOPASSWD: /bin/systemctl status jenkins-agent'

- name: install_jenkins.yml | Copy jenkins-agent.service
  template: 
    src: "jenkins-agent.service.j2"
    dest: "/usr/lib/systemd/system/jenkins-agent.service"
    owner: root
    group: root
    mode: 0644
  register: copy_systemd

- name: install_jenkins.yml | Enable jenkins-agent.service
  systemd:
    name: jenkins-agent
    enabled: yes

- name: install_jenkins.yml | Reload systemd
  command: systemctl daemon-reload
  when: copy_systemd|changed

- name: install_jenkins.yml | Install openjdk for jenkins
  yum:
    name: "java-1.8.0-openjdk"
    state: present

