Playbook is used for the update and patching cognos 10 and 11 Server

Das Playbook installiert die Updates fuer OS, Java und Oracle Instantclient.

Java wird in Zukunft von lsh-vi (HKV) verwaltet.

Reihenfolge der Server
#######################

Wegen den Abhaengigkeiten bei Cognos 10 und 11 sind die Server im Playbook cognos-patch.yml fest eingetragen und es muessen KEINE Limits gesetzt werden.

Vor der Ausfuehrung daher pruefen ob sich an der Struktur bzw. Reihenfolge etwas geaendert hat.
Siehe dazu http://hgf-int-vm-293.int.alte-leipziger.de:9000/mediawiki/index.php/Leitfaden_f%C3%BCr_Betrieb:_Starten_und_Stoppen_von_Cognos_BI#Startreihenfolge

dev - qa - prod Umgebung und cognos Version
###########################################

Die Umgebungen wird mit der Variablen operation bestimmt.
DEV, QA und PROD
Fuer die beiden Cognos Versionen gibt es jeweils eigene Rollen.

########################

20.02.2018

Playbook mit Rollen neu erstellt.
Es gibt jetzt fuer jede Aktion eine eigene Rolle.
Grund fuer die Umstellung war die Prioritaet der Variablen.
Variablen die fuer eine Rolle gelten aber nicht ueberschrieben werden duerfen sind im Ordner vars in der jeweiligen Rolle.
Variablen die ueberschrieben werden koennen bzw. sollen sind im Ornder defaults in der jeweiligen Rolle.

06.02.2018

Playbook laeuft jetzt weiter falls cognos nicht gestartet ist.

17.01.2018

Temporaer das Tag cognos_al hinzugefuegt.
Nur fuers Patchen der AL Cognos Prod Umgebung.
